class BaseError(Exception):
    def __init__(self, msg: str, *args):
        arguments = ', '.join([repr(a) for a in args])

        if len(arguments) > 0:
            msg = f'{msg}: {arguments}'

        super().__init__(msg)


class InvalidArgumentsError(BaseError):
    pass


class ParserError(BaseError):
    pass


class GeometryError(BaseError):
    pass


class SchemaError(BaseError):
    pass
