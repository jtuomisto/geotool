from abc import abstractmethod
from datetime import datetime
from warnings import warn
from typing import Any
from geotool.tools import Counter


class TaxItem:
    _versions = {None}
    name = None

    def __init__(self, version: int):
        if version is None or version not in self._versions:
            raise ValueError(f'Unknown version for {self.__class__.__name__}: {version}')

        self._ver = version

    def _normalize_estate_id(self, estid: str) -> str:
        estid = estid.strip()

        if len(estid) == 0:
            return None

        parts = estid.split('-')
        prop = None
        normalized = []

        if len(parts) == 5:
            prop = parts.pop()

        for p in parts:
            normalized.append(str(int(p)))

        if prop is not None:
            normalized.append(f'M{int(prop[1:])}')

        return '-'.join(normalized)

    def _normalize_date(self, date: str) -> str:
        date = date.strip()
        length = len(date)

        # Data is garbage
        if length == 7:
            date = f'0{date}'
        elif length != 8:
            warn(f'Invalid date string: {date}', RuntimeWarning)
            return None

        try:
            return datetime.strptime(date, '%d%m%Y').strftime('%Y-%m-%d')
        except (OSError, ValueError):
            warn(f'Invalid date string: {date}', RuntimeWarning)

        return None

    def _normalize_fraction(self, n: str, d: str) -> str:
        n, d = n.strip(), d.strip()

        if len(n) == 0 or len(d) == 0:
            return None

        n, d = int(n), int(d)
        return f'{n}/{d}'

    def _normalize(self, value: str, value_type: type=str) -> Any:
        value = value.strip()

        if len(value) == 0:
            return None

        try:
            if value_type is str:
                return value
            elif value_type is float:
                # Data is in Finnish locale
                value = value.replace(',', '.', 1)
            elif value_type is bool:
                value = int(value)

            return value_type(value)
        except (ValueError, TypeError) as e:
            warn(f'Cannot cast value into correct type: {e!s}', RuntimeWarning)

        return None

    @abstractmethod
    def values(self, data: list[str]) -> dict[str, Any]:
        raise NotImplementedError()


class TaxRealEstateItem(TaxItem):
    _versions = {19}
    name = 'real_estates'

    def values(self, data: list[str]) -> dict[str, Any]:
        return {
            'estate_id': self._normalize_estate_id(data[0]),
            'estate_name': self._normalize(data[3]),
            'estate_area': self._normalize(data[4], float),
            'municipality_id': self._normalize(data[1]),
            'municipality_name': self._normalize(data[2])
        }


class TaxOwnerItem(TaxItem):
    _versions = {19, 21}
    name = 'owners'

    def values(self, data: list[str]) -> dict[str, Any]:
        return {
            'estate_id': self._normalize_estate_id(data[0]),
            'terrain_num': self._normalize(data[1], int),
            'building_num': self._normalize(data[2], int),
            'other_profile_num': self._normalize(data[3], int),
            'owner_id': self._normalize(data[4]),
            'owner_name': self._normalize(data[5]),
            'owned_share': self._normalize_fraction(data[6], data[7]),
            'managed_share': self._normalize_fraction(data[8], data[9]),
            'management_rights': self._normalize(data[10], bool),
            'tax_end': self._normalize_date(data[11]),
            'tax_correction': self._normalize_date(data[12])
        }


class TaxTerrainItem(TaxItem):
    _versions = {19, 21}
    name = 'terrains'

    def values(self, data: list[str]) -> dict[str, Any]:
        return {
            'estate_id': self._normalize_estate_id(data[0]),
            'terrain_num': self._normalize(data[1], int),
            'terrain_type': self._normalize(data[2]),
            'terrain_usage': self._normalize(data[3]),
            'terrain_area': self._normalize(data[4], float),
            'building_permit_area': self._normalize(data[5], float),
            'plot_efficiency': self._normalize(data[6], float),
            'plan_type': self._normalize(data[7]),
            'plan_usage': self._normalize(data[8]),
            'plan_unit_id': self._normalize(data[10]),
            'shore': self._normalize(data[9]),
            'calculation_basis': self._normalize(data[11], int),
            'price_area_id': self._normalize(data[12]),
            'discount_formula': self._normalize(data[14], bool),
            'no_estate_tax': self._normalize(data[15], bool),
            'area_price': self._normalize(data[13], float),
            'tax_value': self._normalize(data[16], float),
            'estate_tax': self._normalize(data[17], float)
        }


class TaxBuildingItem(TaxItem):
    _versions = {19}
    name = 'buildings'

    def values(self, data: list[str]) -> dict[str, Any]:
        return {
            'estate_id': self._normalize_estate_id(data[0]),
            'building_num': self._normalize(data[1], int),
            'building_id': self._normalize(data[2]),
            'building_vrk_type': self._normalize(data[3], int),
            'usage': self._normalize(data[4]),
            'demolish_state': self._normalize(data[5], bool),
            'unusable': self._normalize(data[6], bool),
            'no_estate_tax': self._normalize(data[7], bool),
            'common_good': self._normalize(data[8], bool),
            'tax_value': self._normalize(data[9], float),
            'estate_tax': self._normalize(data[10], float)
        }


class TaxBuildingPartItem(TaxItem):
    _versions = {19, 21}
    name = 'building_parts'

    def values(self, data: list[str]) -> dict[str, Any]:
        c = Counter(6)
        vals = {
            'estate_id': self._normalize_estate_id(data[0]),
            'building_num': self._normalize(data[1], int),
            'building_part_num': self._normalize(data[2], int),
            'building_type': self._normalize(data[3]),
            'construction_start': self._normalize_date(data[4]),
            'construction_end': self._normalize_date(data[5]),
            'construction_progress': None
        }

        if self._ver >= 21:
            vals['construction_progress'] = self._normalize(data[c()], float)

        vals.update({
            'building_part_area': self._normalize(data[c()], float),
            'building_part_volume': self._normalize(data[c()], float),
            'supporting_material': self._normalize(data[c()]),
            'renovation_year': self._normalize(data[c()], int),
            'age_discount_year': self._normalize(data[c()], int),
            'unfinished_cellar_area': self._normalize(data[c()], float),
            'electricity': self._normalize(data[c()], bool),
            'heating': self._normalize(data[c()]),
            'water_lines': self._normalize(data[c()], bool),
            'sewer_lines': self._normalize(data[c()], bool),
            'winter_livable': self._normalize(data[c()], bool),
            'porch_area': self._normalize(data[c()], float),
            'toilet': self._normalize(data[c()], bool),
            'sauna': self._normalize(data[c()], bool),
            'building_shape': self._normalize(data[c()]),
            'storage_area': self._normalize(data[c()], float),
            'elevator': self._normalize(data[c()], bool),
            'elevator_shaft_area': self._normalize(data[c()], float),
            'air_conditioning': self._normalize(data[c()], int),
            'heat_and_water': self._normalize(data[c()], int),
            'cellar_area': self._normalize(data[c()], float),
            'parking_area': self._normalize(data[c()], float),
            'silo_type': self._normalize(data[c()]),
            'silo_diameter': self._normalize(data[c()], float),
            'greenhouse_structure_heating': self._normalize(data[c()]),
            'lightweight_canteen': self._normalize(data[c()], bool),
            'car_park_roof': self._normalize(data[c()], bool),
            'insulation': self._normalize(data[c()], bool)
        })

        return vals


class TaxOtherProfileItem(TaxItem):
    _versions = {19}
    name = 'other_profiles'

    def values(self, data: list[str]) -> dict[str, Any]:
        return {
            'estate_id': self._normalize_estate_id(data[0]),
            'profile_num': self._normalize(data[1], int),
            'profile_type': self._normalize(data[2]),
            'hydropower_value': self._normalize(data[3], float),
            'tax_value': self._normalize(data[4], float),
            'estate_tax': self._normalize(data[5], float)
        }
