import sqlite3
from typing import Any


# TODO: Monetary values should not be REAL
INIT_DB_SQL = """
CREATE TABLE IF NOT EXISTS real_estates(
    estate_id TEXT,
    estate_name TEXT,
    estate_area REAL,
    municipality_id TEXT,
    municipality_name TEXT
);
CREATE INDEX IF NOT EXISTS real_estate_ind ON real_estates(estate_id);

CREATE TABLE IF NOT EXISTS owners(
    estate_id TEXT,
    terrain_num INT,
    building_num INT,
    other_profile_num INT,
    owner_id TEXT,
    owner_name TEXT,
    owned_share TEXT,
    managed_share TEXT,
    management_rights BOOLEAN,
    tax_end DATE,
    tax_correction DATE
);
CREATE INDEX IF NOT EXISTS owner_ind ON owners(estate_id);

CREATE TABLE IF NOT EXISTS terrains(
    estate_id TEXT,
    terrain_num INT,
    terrain_type TEXT,
    terrain_usage TEXT,
    terrain_area REAL,
    building_permit_area REAL,
    plot_efficiency REAL,
    plan_type TEXT,
    plan_usage TEXT,
    plan_unit_id TEXT,
    shore TEXT,
    calculation_basis INT,
    price_area_id TEXT,
    discount_formula BOOLEAN,
    no_estate_tax BOOLEAN,
    area_price REAL,
    tax_value REAL,
    estate_tax REAL
);
CREATE INDEX IF NOT EXISTS terrain_ind ON terrains(estate_id);

CREATE TABLE IF NOT EXISTS buildings(
    estate_id TEXT,
    building_num INT,
    building_id TEXT,
    building_vrk_type INT,
    usage TEXT,
    demolish_state BOOLEAN,
    unusable BOOLEAN,
    no_estate_tax BOOLEAN,
    common_good BOOLEAN,
    tax_value REAL,
    estate_tax REAL
);
CREATE INDEX IF NOT EXISTS building_ind ON buildings(estate_id);
CREATE INDEX IF NOT EXISTS building_id_ind ON buildings(building_id);

CREATE TABLE IF NOT EXISTS building_parts(
    estate_id TEXT,
    building_num INT,
    building_part_num INT,
    building_type TEXT,
    construction_start DATE,
    construction_end DATE,
    construction_progress REAL,
    building_part_area REAL,
    building_part_volume REAL,
    supporting_material TEXT,
    renovation_year INT,
    age_discount_year INT,
    unfinished_cellar_area REAL,
    electricity BOOLEAN,
    heating TEXT,
    water_lines BOOLEAN,
    sewer_lines BOOLEAN,
    winter_livable BOOLEAN,
    porch_area REAL,
    toilet BOOLEAN,
    sauna BOOLEAN,
    building_shape TEXT,
    storage_area REAL,
    elevator BOOLEAN,
    elevator_shaft_area REAL,
    air_conditioning INT,
    heat_and_water INT,
    cellar_area REAL,
    parking_area REAL,
    silo_type TEXT,
    silo_diameter REAL,
    greenhouse_structure_heating TEXT,
    lightweight_canteen BOOLEAN,
    car_park_roof BOOLEAN,
    insulation BOOLEAN
);
CREATE INDEX IF NOT EXISTS building_part_ind ON building_parts(estate_id);

CREATE TABLE IF NOT EXISTS other_profiles(
    estate_id TEXT,
    profile_num INT,
    profile_type TEXT,
    hydropower_value REAL,
    tax_value REAL,
    estate_tax REAL
);
CREATE INDEX IF NOT EXISTS other_profile_ind ON other_profiles(estate_id);

CREATE VIEW IF NOT EXISTS buildings_view AS
SELECT
buildings.*,
onames.owner_names,
bparts.building_area,
bparts.building_volume,
bparts.building_date
FROM buildings
LEFT JOIN (
    SELECT
    estate_id,
    building_num,
    sum(building_part_area) as building_area,
    sum(building_part_volume) as building_volume,
    max(ifnull(construction_start, construction_end)) as building_date
    FROM building_parts
    WHERE estate_id IS NOT NULL
    GROUP BY estate_id, building_num
) bparts
ON buildings.estate_id = bparts.estate_id AND buildings.building_num = bparts.building_num
LEFT JOIN (
    SELECT estate_id, group_concat(DISTINCT owner_name) as owner_names
    FROM owners
    WHERE estate_id IS NOT NULL
    GROUP BY estate_id
) onames
ON buildings.estate_id = onames.estate_id
WHERE buildings.estate_id IS NOT NULL;

CREATE VIEW IF NOT EXISTS real_estates_view AS
SELECT
real_estates.*,
tr.usage,
tr.sum_value,
tr.sum_tax,
onames.owner_names
FROM real_estates
LEFT JOIN (
    SELECT
    estate_id,
    group_concat(DISTINCT plan_usage) as usage,
    sum(tax_value) as sum_value,
    sum(estate_tax) as sum_tax
    FROM terrains
    WHERE estate_id IS NOT NULL
    GROUP BY estate_id
) tr
ON real_estates.estate_id = tr.estate_id
LEFT JOIN (
    SELECT estate_id, group_concat(DISTINCT owner_name) as owner_names
    FROM owners
    WHERE estate_id IS NOT NULL
    GROUP BY estate_id
) onames
ON real_estates.estate_id = onames.estate_id;
"""


class TaxesDB:
    def __init__(self):
        self._conn = None

    def __del__(self):
        self.close()

    def _init_db(self):
        self._conn.executescript(INIT_DB_SQL)

    def close(self):
        self.end_transaction()
        self._conn.close()

    def connect(self, conn_file: str):
        if self._conn is not None:
            self.close()

        self._conn = sqlite3.connect(conn_file, isolation_level=None)
        self._init_db()

    def start_transaction(self):
        if self._conn is None or self._conn.in_transaction:
            return

        self._conn.execute('BEGIN DEFERRED TRANSACTION;')

    def end_transaction(self):
        if self._conn is None or not self._conn.in_transaction:
            return

        # Is this necessary?
        self._conn.execute('COMMIT TRANSACTION;')
        self._conn.commit()

    def insert(self, table: str, values: dict[str, Any]):
        if self._conn is None:
            raise RuntimeError('No connection')

        columns = ','.join(values.keys())
        qmarks = ','.join('?' * len(values))
        sql = f'INSERT INTO {table} ({columns}) VALUES ({qmarks});'

        try:
            self._conn.execute(sql, tuple(values.values()))
        except sqlite3.ProgrammingError as e:
            raise ValueError('Invalid columns/values') from e
        except sqlite3.DatabaseError as e:
            raise RuntimeError('Error in database') from e
