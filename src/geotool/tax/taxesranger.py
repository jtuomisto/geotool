import csv
from typing import Callable
from geotool.errors import ParserError
from .taxesdb import TaxesDB
from .taxitem import (TaxRealEstateItem, TaxOwnerItem, TaxTerrainItem,
                      TaxBuildingItem, TaxBuildingPartItem, TaxOtherProfileItem)


class TaxesRanger:
    def __init__(self, csv_filepath: str):
        self._db = TaxesDB()
        self._csv = csv_filepath
        self._progress = lambda extra_info=None: None
        self._encoding = 'ansi'
        self._dialect = {
            'delimiter': ';',
            'quoting': csv.QUOTE_NONE
        }

    def _insert_line(self, csv_items: list):
        full_item_type, *data = csv_items

        try:
            item_type, _, version = full_item_type.rpartition('.')
            version = int(version)
        except (ValueError, TypeError, IndexError) as e:
            raise ParserError('Unknown tax item type', full_item_type) from e

        match item_type:
            case '01.01.02':
                # Summary of the tax data
                if data[0].strip().lower() != 'real estate':
                    raise ParserError('Only real estate tax files are supported')

                return
            case '12.06.02':
                item = TaxRealEstateItem(version)
            case '12.07.03':
                item = TaxOwnerItem(version)
            case '12.08.05':
                item = TaxTerrainItem(version)
            case '12.09.04':
                item = TaxBuildingItem(version)
            case '12.10.03':
                item = TaxBuildingPartItem(version)
            case '12.11.03':
                item = TaxOtherProfileItem(version)
            case _:
                raise ParserError('Unknown tax item type', item_type)

        try:
            self._db.insert(item.name, item.values(data))
        except IndexError as e:
            raise ParserError('Invalid csv data, missing column') from e

    def set_progress_func(self, func: Callable):
        self._progress = func

    def write_to_db(self, output: str):
        self._db.connect(output)
        self._db.start_transaction()

        with open(self._csv, mode='r', newline='', encoding=self._encoding) as handle:
            reader = csv.reader(handle, **self._dialect)

            for line in reader:
                self._insert_line(line)
                self._progress()

        self._db.end_transaction()
