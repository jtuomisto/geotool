from functools import partial
from warnings import warn
from geotool.geometries import GeoPoint


# Commands available in comments

# Single integer value
def surface_value(command: str, gp: GeoPoint):
    gp['surface'] = int(command)


# Coordinate offset: (X|Y|Z)OFF (-|+)number
def coord_offset(command: str, args: list[str], gp: GeoPoint):
    param = f'{command[0].lower()}_off'
    offset = ''.join(args)

    try:
        offset = float(offset)
    except (ValueError, TypeError):
        return

    offset_args = {'x_off': 0, 'y_off': 0, 'z_off': 0}
    offset_args[param] = offset
    gp.offset(**offset_args)


class GTCommentParser:
    def __init__(self):
        self._steps = []

    @property
    def have_commands(self) -> bool:
        return len(self._steps) > 0

    def is_comment(self, line: str) -> bool:
        return line.startswith('!')

    def clear(self):
        self._steps.clear()

    def append(self, line: str):
        if not self.is_comment(line):
            return

        comment = line[1:].lstrip()

        if len(comment) == 0:
            return

        command, *args = comment.lower().split()
        step = None

        match command:
            case 'xoff' | 'yoff' | 'zoff':
                step = partial(coord_offset, command, args)
            case _ if command.isnumeric():
                step = partial(surface_value, command)
            case _:
                warn(f'Skipping comment: {comment}', RuntimeWarning)

        if step is not None:
            self._steps.append(step)

    def run_commands(self, gp: GeoPoint):
        if len(self._steps) == 0:
            return

        for f in self._steps:
            f(gp)

        self._steps.clear()
