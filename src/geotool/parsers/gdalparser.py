from warnings import warn
from typing import Iterator
from osgeo import gdal, ogr
from geotool.schema import GeoSchema, GeoLayer, GeoAttribute
from geotool.schema.types import SchemaInt32, SchemaOGRDateTime
from geotool.tools import get_global_crs
from geotool.errors import GeometryError
from geotool.geometries import (BaseGeometry, GeoContainer, get_geo_from_ogr,
                                coerce_geometry, geom_is_2D)
from .geoparser import GeoParser


FIELD_TYPE_TO_NATIVE = {
    ogr.OFTInteger: SchemaInt32,
    ogr.OFTInteger64: int,
    ogr.OFTReal: float,
    ogr.OFTString: str,
    ogr.OFTDate: SchemaOGRDateTime,
    ogr.OFTTime: SchemaOGRDateTime,
    ogr.OFTDateTime: SchemaOGRDateTime,
    (ogr.OFTInteger, ogr.OFSTBoolean): bool,
    (ogr.OFTInteger64, ogr.OFSTBoolean): bool  # is this even a possible type?
    # TODO: Maybe implement these
    # ogr.OFTIntegerList
    # ogr.OFTInteger64List
    # ogr.OFTRealList
    # ogr.OFTStringList
    # ogr.OFTWideString
    # ogr.OFTWideStringList
    # ogr.OFTBinary
}


class GDALParser(GeoParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._dataset = None
        self._crs = None
        self._schema = None

    def _create_feature(self, feature: ogr.Feature, schema_layer: GeoLayer) -> BaseGeometry:
        if (geom := feature.GetGeometryRef()) is None:
            warn(f'Skipped feature with no geometry in layer "{schema_layer.name}"', RuntimeWarning)
            return None

        try:
            g = get_geo_from_ogr(geom)
            g = coerce_geometry(g, schema_layer.geometry_type)
        except GeometryError as e:
            gn, ln, lgn = geom.GetGeometryName(), schema_layer.name, schema_layer.geometry_name
            warn(f'Unable to create geometry ({gn}, {lgn}) in layer "{ln}": {e!s}', RuntimeWarning)
            return None

        g.set_crs(self._crs)
        g.set_layer(schema_layer.name)

        for attr in schema_layer.attributes():
            fid = attr.get_meta('in_field_index')

            if attr.var_type is SchemaOGRDateTime:
                value = feature.GetFieldAsDateTime(fid)
            else:
                value = feature.GetField(fid)

            g[attr.name] = attr.try_cast(value)

        return g

    def _build_schema(self) -> GeoSchema:
        schema = GeoSchema()
        layer_count = self._dataset.GetLayerCount()

        for i in range(layer_count):
            layer = self._dataset.GetLayerByIndex(i)
            layer_geom = layer.GetGeomType()
            defn = layer.GetLayerDefn()
            geo_type = get_geo_from_ogr(layer_geom)
            schema_layer = GeoLayer(layer.GetName(), geo_type, is_2D=geom_is_2D(layer_geom))
            field_count = defn.GetFieldCount()

            for j in range(field_count):
                field = defn.GetFieldDefn(j)
                main_type, sub_type = field.GetType(), field.GetSubType()
                attr_type = FIELD_TYPE_TO_NATIVE.get((main_type, sub_type))

                if attr_type is None:
                    attr_type = FIELD_TYPE_TO_NATIVE.get(main_type)

                    if attr_type is None:
                        continue

                schema_attribute = GeoAttribute(field.GetName(), attr_type)
                schema_attribute.set_meta('in_field_index', j)
                schema_layer.add_attribute(schema_attribute)

            schema.add_layer(schema_layer)

        return schema

    def _reader(self) -> Iterator[BaseGeometry]:
        default_crs = self.get('crs')
        dataset_crs = self._dataset.GetSpatialRef()
        layer_count = self._dataset.GetLayerCount()

        if dataset_crs is not None:
            dataset_crs = get_global_crs(dataset_crs)

        for i in range(layer_count):
            layer = self._dataset.GetLayerByIndex(i)
            layer_crs = layer.GetSpatialRef()
            sl = self._schema.get_layer(layer.GetName())

            if layer_crs is not None:
                self._crs = get_global_crs(layer_crs)
            elif dataset_crs is not None:
                self._crs = dataset_crs
            else:
                self._crs = default_crs

            while (feature := layer.GetNextFeature()) is not None:
                if (f := self._create_feature(feature, sl)) is None:
                    continue

                yield f

        self._dataset = None

    def parse(self, filepath: str) -> GeoContainer:
        self._dataset = gdal.OpenEx(filepath, gdal.OF_VECTOR | gdal.OF_READONLY)
        self._schema = self._build_schema()
        return GeoContainer(self._schema, self._reader(), None)
