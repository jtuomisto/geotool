from typing import Iterator
from itertools import chain
from geotool.geometries import BaseGeometry, GeoPoint, GeoLine, GeoContainer
from geotool.errors import ParserError
from .geoparser import GeoParser
from .gtcommentparser import GTCommentParser


class GTParser(GeoParser):
    def _parse_codes(self, arg: str) -> list[str]:
        if arg is None:
            return []

        filtered = filter(None, map(lambda x: x.strip(), arg.split(',')))
        return [c for c in filtered]

    def _parse_line(self, line: str, crs) -> GeoPoint:
        data = []
        acc = 0

        for i in (8, 8, 8, 8, 14, 14, 14):
            try:
                piece = line[acc:acc + i].strip()
            except IndexError as e:
                raise ParserError('Missing gt data', line) from e

            data.append(piece)
            acc += i

        try:
            surface, gt_line, code, point = data[:4]
            coords = data[4:]
            gp = GeoPoint(*coords, crs=crs)

            attrs = {
                'surface': '0' if len(surface) == 0 else surface,
                'line': '0' if len(gt_line) == 0 else gt_line,
                'code': '0' if len(code) == 0 else code,
                'point': '0' if len(point) == 0 else point
            }
        except (IndexError, TypeError, ValueError) as e:
            raise ParserError('Invalid gt data') from e

        gp.update_attributes(attrs)
        return gp

    def _read_points(self, filepath: str) -> tuple[list[GeoPoint], int]:
        crs = self.get('crs')
        parse_comments = self.get('gt-comment-hacks')
        comment_parser = GTCommentParser()
        last = None
        max_line_value = 0
        points = []

        with self._open(filepath) as handle:
            for line in handle:
                stripped = line.strip()

                if len(stripped) == 0:
                    continue

                if comment_parser.is_comment(stripped):
                    if parse_comments:
                        comment_parser.append(stripped)

                    continue

                points.append(self._parse_line(line, crs))

                if last is None:
                    comment_parser.clear()
                elif comment_parser.have_commands:
                    comment_parser.run_commands(last)

                last = points[-1]

                if last['line'].isnumeric():
                    max_line_value = max(max_line_value, int(last['line']))

        if comment_parser.have_commands and last is not None:
            comment_parser.run_commands(last)

        return points, max_line_value

    def _construct_geometries(self, filepath: str) -> tuple[list[GeoPoint], list[GeoLine]]:
        line_codes = set(self._parse_codes(self.get('gt-code-as-line')))
        no_lines = self.get('no-lines')
        points, max_line_value = self._read_points(filepath)
        simple_points = []
        lines = {}
        last_line_code = None

        for gp in points:
            code_value = gp['code']
            line_value = (code_value, gp['line'])
            is_line = code_value in line_codes

            if no_lines or (line_value[1] == '0' and not is_line):
                last_line_code = None
                gp.set_layer('points')
                simple_points.append(gp)
                continue
            elif line_value[1] == '0' and is_line:
                if last_line_code is None or last_line_code != code_value:
                    gline = GeoLine(crs=gp.crs, layer='lines')
                    gline.update_attributes(gp)
                    gline.append(gp)
                    last_line_code = code_value
                    max_line_value += 1
                    gline['line'] = str(max_line_value)
                    lines[max_line_value] = gline
                else:
                    lines[max_line_value].append(gp)

                continue

            last_line_code = None

            if (gline := lines.get(line_value)) is not None:
                gline.append(gp)
            else:
                gline = GeoLine(crs=gp.crs, layer='lines')
                gline.update_attributes(gp)
                gline.append(gp)
                lines[line_value] = gline

        delete_keys = []

        for ln_value, ln in lines.items():
            if len(ln) > 1:
                continue

            x, y, z = ln.geometry.GetPoint()
            point = GeoPoint(y, x, z, ln.crs, 'points')
            point.update_attributes(ln)
            point['line'] = 0
            simple_points.append(point)
            delete_keys.append(ln_value)

        for k in delete_keys:
            lines.pop(k)

        return simple_points, lines.values()

    def parse(self, filepath: str) -> GeoContainer:
        code_layers = self.get('code-layers')
        points, lines = self._construct_geometries(filepath)

        if not code_layers:
            schema = self._build_basic_schema()
        else:
            schema = self._build_schema(points, lines)

        return GeoContainer(schema, chain(points, lines), None)
