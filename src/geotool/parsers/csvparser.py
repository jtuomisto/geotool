import csv
from typing import Iterator
from geotool.geometries import BaseGeometry, GeoPoint, GeoLine, GeoContainer
from geotool.errors import ParserError, InvalidArgumentsError
from .geoparser import GeoParser


class CSVParser(GeoParser):
    def _reader(self, filepath: str, csv_order: list) -> Iterator[BaseGeometry]:
        crs = self.get('crs')
        no_lines = self.get('no-lines')
        item_count = len(csv_order)
        glines = {}

        with self._open(filepath) as handle:
            sample = handle.read(1024)
            handle.seek(0)

            dialect = csv.Sniffer().sniff(sample)
            header = csv.Sniffer().has_header(sample)

            if header:
                _ = handle.readline()

            reader = csv.reader(handle, dialect)

            for line in reader:
                data = [v.strip() for v in filter(None, line)]

                if len(data) != item_count:
                    raise ParserError('Missing csv data', line)

                p = {
                    'surface': None,
                    'line': None,
                    'code': None,
                    'point': None,
                    'north': None,
                    'east': None,
                    'elevation': None
                }
                p.update(zip(csv_order, data))
                north = p.pop('north')
                east = p.pop('east')
                elev = p.pop('elevation')

                try:
                    gp = GeoPoint(north, east, elev, crs=crs)
                    p['surface'] = '0' if p['surface'] is None else p['surface']
                    p['line'] = '0' if p['line'] is None else p['line']
                    p['code'] = '0' if p['code'] is None else p['code']
                    p['point'] = '0' if p['point'] is None else p['point']
                except (TypeError, ValueError) as e:
                    raise ParserError('Invalid csv data') from e

                line_value = (p['code'], p['line'])

                if no_lines or line_value[1] == '0':
                    gp.set_layer('points')
                    gp.update_attributes(p)
                    yield gp
                    continue

                if (gline := glines.get(line_value)) is not None:
                    gline.append(gp)
                else:
                    gline = GeoLine(crs=gp.crs, layer='lines')
                    gline.update_attributes(p)
                    gline.append(gp)
                    glines[line_value] = gline

        for ln in glines.values():
            if len(ln) > 1:
                yield ln
                continue

            x, y, z = ln.geometry.GetPoint()
            point = GeoPoint(y, x, z, ln.crs, 'points')
            point.update_attributes(ln)
            point['line'] = 0

            yield point

    def parse(self, filepath: str) -> GeoContainer:
        code_layers = self.get('code-layers')
        columns = self.get('csv-columns')
        default_order = ['surface', 'line', 'code', 'point', 'north', 'east', 'elevation']
        csv_order = default_order

        if columns is not None:
            csv_order = [x.strip().lower() for x in columns.split(',')]
            set_order = set(csv_order)

            if len(csv_order) == 0 or len(csv_order) != len(set_order):
                raise InvalidArgumentsError('Invalid or duplicate csv items', columns)

            if not set_order.issubset(set(default_order)):
                raise InvalidArgumentsError('Unknown csv items', columns)

        reader = self._reader(filepath, csv_order)

        if not code_layers:
            schema = self._build_basic_schema()
            return GeoContainer(schema, reader, None)
        else:
            geoms = [g for g in reader]
            schema = self._build_schema(geoms)
            return GeoContainer(schema, geoms, None)
