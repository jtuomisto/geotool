from abc import abstractmethod, ABC
from typing import Any, Iterable
from itertools import chain
from geotool.geometries import BaseGeometry, GeoPoint, GeoLine, GeoContainer
from geotool.schema import GeoSchema, GeoLayer, GeoAttribute


class GeoParser(ABC):
    def __init__(self, options: dict={}):
        self._opt = {**options}

    def _open(self, filepath: str):
        return open(filepath, mode='r', newline='', encoding=self.get('encoding', 'utf8'))

    def _build_basic_schema(self) -> GeoSchema:
        schema = GeoSchema()
        pl = GeoLayer('points', GeoPoint)
        ll = GeoLayer('lines', GeoLine)
        surface_attr = GeoAttribute('surface', str)
        line_attr = GeoAttribute('line', str)
        code_attr = GeoAttribute('code', str)
        point_attr = GeoAttribute('point', str)

        pl.add_attribute(surface_attr, line_attr, code_attr, point_attr)
        ll.add_attribute(surface_attr, line_attr, code_attr, point_attr)
        schema.add_layer(pl, ll)

        return schema

    def _build_schema(self, *args: Iterable[BaseGeometry]) -> GeoSchema:
        schema = GeoSchema()

        for g in chain.from_iterable(args):
            code = g['code']
            geom_name = 'point' if isinstance(g, GeoPoint) else 'line'
            layer_name = f'{geom_name}_{code}'
            g.set_layer(layer_name)

            if schema.get_layer(layer_name) is not None:
                continue

            layer = GeoLayer(layer_name, g.__class__)
            layer.add_attribute(GeoAttribute('surface', str),
                                GeoAttribute('line', str),
                                GeoAttribute('code', str),
                                GeoAttribute('point', str))
            schema.add_layer(layer)

        return schema

    def get(self, opt: str, default: Any=None) -> Any:
        if (value := self._opt.get(opt, default)) is None:
            return default

        return value

    @abstractmethod
    def parse(self, filepath: str) -> GeoContainer:
        raise NotImplementedError()
