import argparse
import glob
import sys
import warnings
import traceback
from datetime import datetime
from os.path import basename
from osgeo import gdal, ogr, osr
import geotool.commands as commands
from geotool.tools import get_global_crs
from geotool.misc import parse_layer_map
from geotool.log import print_err, print_warn


DEFAULT_CRS = 'EPSG:3877'
CSV_COLUMNS = 'surface, line, code, point, north, east, elevation'
TYPES_CHOICES = ['csv', 'gt', 'geojson', 'gpkg', 'pgsql', 'gcp', 'shape']
TYPES_LIST = ', '.join(TYPES_CHOICES)
SPATIAL_CHOICES = ['none', 'gist', 'spgist', 'brin']
SPATIAL_LIST = ', '.join(SPATIAL_CHOICES)
DROP_CHOICES = ['no', 'yes', 'if_exists']
DROP_LIST = ', '.join(DROP_CHOICES)
CLI_COMMANDS = {
    'join': commands.join,
    'format': commands.join,
    'concat': commands.join,
    'replace': commands.replace,
    'vacuum': commands.vacuum,
    'read-ktj': commands.read_ktj,
    'format-ktj': commands.read_ktj,
    'download-ktj': commands.download_ktj,
    'concat-ktj': commands.concat_ktj,
    'estate-tax-db': commands.estate_tax_db
}
DESCRIPTION = 'GIS-data helper tool'
EPILOG = """Format support:
GT:             read/write
CSV:            read/write
GeoJSON:        read/write
GeoPackage:     read/write
ESRI Shapefile: read/write
Postgres dump:  write"""


def parse_args(arguments):
    frmt_class = argparse.RawTextHelpFormatter
    pr = argparse.ArgumentParser(description=DESCRIPTION, epilog=EPILOG,
                                 allow_abbrev=False, formatter_class=frmt_class)

    # Common arguments
    pr.add_argument('-t', '--output-type',
                    type=str, default='geojson', choices=TYPES_CHOICES, metavar='TYPE',
                    help=f'Output type ({TYPES_LIST})')
    pr.add_argument('-o', '--output',
                    type=str, metavar='OUTPUT',
                    help='Output file or database connection string')
    pr.add_argument('-F', '--from-crs',
                    type=get_global_crs, default=DEFAULT_CRS, metavar='CRS',
                    help='CRS of input files')
    pr.add_argument('-T', '--to-crs',
                    type=get_global_crs, default=DEFAULT_CRS, metavar='CRS',
                    help='CRS of output files')
    pr.add_argument('-a', '--auth',
                    type=str, metavar='USER:PASS',
                    help='Username and password, separated by colon')
    pr.add_argument('-R', '--renumber',
                    action='store_true',
                    help='Renumber points starting from 1')
    pr.add_argument('-u', '--unique',
                    action='store_true',
                    help='Forces point numbers to be unique by appending a counter to duplicates')
    pr.add_argument('-e', '--encoding',
                    type=str, default='utf8', metavar='ENCODING',
                    help='Encoding of input text files i.e. gt, csv and layer mapping file')
    pr.add_argument('--layer-map',
                    type=str, metavar='FILE',
                    help='Text file containing layer name mapping, used for renaming layers')
    pr.add_argument('--no-glob',
                    action='store_true',
                    help='Do not glob given paths')

    # CLI output arguments
    pr.add_argument('--debug',
                    action='store_true',
                    help='Debug output')
    pr.add_argument('--no-progress',
                    action='store_true',
                    help='Disable progress bar')

    # Read/write arguments for formats
    pr.add_argument('--write-vertex',
                    action='store_true',
                    help='(all formats) Write line and polygon vertices as points')
    pr.add_argument('--linear',
                    action='store_true',
                    help='(all formats) Transform curved features into linear features')
    pr.add_argument('--no-lines',
                    action='store_true',
                    help='(gt, csv) Do not produce lines')
    pr.add_argument('--code-layers',
                    action='store_true',
                    help='(gt, csv) Separate features into layers based on their code')
    pr.add_argument('--gt-comment-hacks',
                    action='store_true',
                    help='(gt) Parse commands from comments')
    pr.add_argument('--gt-code-as-line',
                    type=str, metavar='CODES',
                    help='(gt) Comma separated list of codes to handle as lines')
    pr.add_argument('--csv-columns',
                    type=str, metavar='COLUMNS',
                    help=f'(csv) Order of columns in file, comma separated list ({CSV_COLUMNS})')
    pr.add_argument('--pgsql-schema',
                    type=str, default='public', metavar='NAME',
                    help='(pgsql) Schema name')
    pr.add_argument('--pgsql-spatial',
                    type=str, default='gist', choices=SPATIAL_CHOICES, metavar='METHOD',
                    help=f'(pgsql) Spatial index method used by PostGIS ({SPATIAL_LIST})')
    pr.add_argument('--pgsql-insert',
                    action='store_true',
                    help='(pgsql) Use INSERT, default is COPY')
    pr.add_argument('--pgsql-no-create',
                    action='store_true',
                    help='(pgsql) Disable schema and table creation')
    pr.add_argument('--pgsql-drop',
                    type=str, default='if_exists', choices=DROP_CHOICES, metavar='DROP',
                    help=f'(pgsql) Emit DROP TABLE ({DROP_LIST})')

    # Subparsers
    spr = pr.add_subparsers(dest='command', required=True)

    join_parser = spr.add_parser('join', aliases=('format', 'concat'),
                                 help='format/concatenate multiple files')
    replace_parser = spr.add_parser('replace',
                                    help='replace code of an item')
    vacuum_parser = spr.add_parser('vacuum',
                                   help='remove layer attributes that are null for all layer items')
    ktj_parser = spr.add_parser('read-ktj', aliases=('format-ktj',),
                                help='read MML KTJ-data from a directory')
    ktj_dl_parser = spr.add_parser('download-ktj',
                                   help='download KTJ-data from MML into a directory')
    ktj_concat_parser = spr.add_parser('concat-ktj',
                                       help='concatenate KTJ-data into a singe xml-file')
    est_tax_parser = spr.add_parser('estate-tax-db',
                                    help='save finnish estate tax (csv) as a sqlite database')

    # Join/format/concat arguments
    join_parser.add_argument('files',
                             nargs='+',
                             help='input file(s)')

    # Replace arguments
    replace_parser.add_argument('-c', '--change',
                                type=str, action='append', required=True, metavar='STRING',
                                help='string of form attribute:old_value:new_value, can be given multiple times')
    replace_parser.add_argument('files',
                                nargs='+',
                                help='input file(s)')

    # Vacuum arguments
    vacuum_parser.add_argument('file',
                               help='input file')

    # KTJ-parser arguments
    ktj_parser.add_argument('directory',
                            help='input directory with zipped ktj-data')

    # KTJ-downloader arguments
    ktj_dl_parser.add_argument('-i', '--id',
                               type=str, required=True, metavar='ID',
                               help='KTJ id string')
    ktj_dl_parser.add_argument('directory',
                               help='directory where to save ktj-data')

    # KTJ-concatenator arguments
    ktj_concat_parser.add_argument('-m', '--millimeters',
                                   action='store_true',
                                   help='convert precision values to millimeters')
    ktj_concat_parser.add_argument('directory',
                                   help='input directory with zipped ktj-data')

    # Estate tax arguments
    est_tax_parser.add_argument('file',
                                help='input file')

    return pr.parse_args(arguments)


def show_warning(message, category, filename, lineno, file=None, line=None):
    print_warn('{}: {} ({}:{})', category.__name__, message, basename(filename), lineno)


def get_first_file(args) -> str:
    first_file = None

    if hasattr(args, 'file') and args.file is not None:
        first_file = args.file
    elif hasattr(args, 'files') and args.files is not None and len(args.files) > 0:
        first_file = args.files[0]

    return first_file


def main(*arguments):
    # By default gdal writes errors to stdout
    gdal.PushErrorHandler('CPLQuietErrorHandler')
    # Is it necessary to call all of these?
    gdal.UseExceptions()
    ogr.UseExceptions()
    osr.UseExceptions()

    warnings.showwarning = show_warning

    if len(arguments) == 0:
        arguments = sys.argv[1:]

    try:
        args = parse_args(arguments)
    except Exception as e:
        print_err('{}: Failed to parse arguments, {}', type(e).__name__, e)
        sys.exit(2)

    if not args.no_glob and hasattr(args, 'files') and args.files is not None:
        files = []

        for f in args.files:
            files.extend(glob.glob(f))

        args.files = list(set(files))  # Remove duplicate files

    if args.output is None:
        dt = datetime.now().strftime('%Y%m%d_%H%M%S')
        filename = get_first_file(args)

        if filename is not None:
            filename, _, _ = filename.rpartition('.')
            args.output = f'{basename(filename)}_{dt}'
        else:
            args.output = f'{args.command}_{dt}'

    try:
        if args.layer_map is not None:
            args.layer_map = parse_layer_map(args.layer_map, args.encoding)

        CLI_COMMANDS[args.command](args)
    except Exception as e:
        if args.debug and e.__cause__ is not None:
            print_err('{}: {}', type(e.__cause__).__name__, e.__cause__)
            traceback.print_tb(e.__cause__.__traceback__)

        print_err('{}: {}', type(e).__name__, e)

        if args.debug:
            traceback.print_tb(e.__traceback__)

        sys.exit(1)
