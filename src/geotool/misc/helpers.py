from contextlib import closing
from getpass import getpass
from typing import Callable
from geotool.geometries import GeoPoint
from geotool.tools import Counter
from geotool.errors import InvalidArgumentsError


RENUMBER_COUNTER = Counter(1)
UNIQUE_DICT = {}


# Check if user/pass was given, ask if not
def check_auth(args):
    if args.auth is not None:
        u, _, p = args.auth.partition(':')
    else:
        u = input('Username: ')
        p = getpass('Password: ')

    args.auth = (u if len(u) > 0 else None, p if len(p) > 0 else None)


# Decorator for commands that need authentication
def needs_auth(func: Callable) -> Callable:
    def with_auth_check(args):
        check_auth(args)
        return func(args)

    return with_auth_check


# Renumbers point-attributes, really only useful when dealing with csv and gt
def renumber_feature(g):
    if isinstance(g, GeoPoint):
        g['point'] = RENUMBER_COUNTER()

    return g


# Makes point-attributes unique by appending a counter, only useful with csv and gt
def unique_point(g):
    if not isinstance(g, GeoPoint):
        return g

    gp = '0' if g['point'] is None else g['point']

    if gp in UNIQUE_DICT:
        tmp_point = f'{gp}_{UNIQUE_DICT[gp]}'

        while tmp_point in UNIQUE_DICT:
            UNIQUE_DICT[gp] += 1
            tmp_point = f'{gp}_{UNIQUE_DICT[gp]}'

        gp = tmp_point

    g['point'] = gp
    UNIQUE_DICT[gp] = 0

    return g


# Parses file given by --layer-map
def parse_layer_map(filepath: str, encoding: str='utf8') -> dict:
    try:
        handle = open(filepath, mode='r', encoding=encoding)
    except LookupError as e:
        raise InvalidArgumentsError('Unknown encoding', encoding) from e

    with closing(handle) as h:
        lines = [ln for ln in map(lambda s: s.strip(), h.readlines()) if len(ln) > 0]

    count = len(lines)
    lmap = {}

    if count % 2 != 0:
        raise InvalidArgumentsError('Layer mapping file must have an even number of lines')

    for i in range(0, count - 1, 2):
        from_name, to_name = lines[i].strip(), lines[i + 1].strip()

        if from_name in lmap or to_name in lmap:
            raise InvalidArgumentsError('Duplicate layer names in layer mapping file')

        lmap[from_name] = to_name

    return lmap
