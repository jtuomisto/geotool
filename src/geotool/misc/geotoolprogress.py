import time
from typing import Callable
from rich.progress import Progress, BarColumn, TextColumn, TimeElapsedColumn
from geotool.log import CON_STDOUT


class GeotoolProgress:
    def __init__(self, description: str):
        self._progress = Progress(BarColumn(16),
                                  TextColumn('({task.fields[counter]})'),
                                  TextColumn('{task.description}... '),
                                  TimeElapsedColumn(),
                                  TextColumn(' {task.fields[extra_info]}'),
                                  console=CON_STDOUT,
                                  transient=True)
        self._task = self._progress.add_task(description, start=False, counter=0, extra_info='')
        self._counter = 0
        self._finished = False

    def __del__(self):
        self.finish()

    def _prog_func(self, *args, extra_info: str=None):
        comp = int((time.monotonic() % 1) * 100)
        self._counter += 1

        if extra_info is None:
            self._progress.update(self._task, completed=comp, counter=self._counter)
        else:
            self._progress.update(self._task, completed=comp, counter=self._counter, extra_info=extra_info)

    def setup(self) -> Callable:
        self._progress.start()
        self._progress.start_task(self._task)
        return self._prog_func

    def finish(self):
        if self._finished:
            return

        self._finished = True
        self._progress.stop_task(self._task)
        self._progress.stop()
        self._progress.remove_task(self._task)
