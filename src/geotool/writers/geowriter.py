from abc import abstractmethod, ABC
from typing import Any
from geotool.geometries import GeoContainer


class GeoWriter(ABC):
    def __init__(self, options: dict={}):
        self._opt = {**options}

    def _open(self, filepath: str):
        return open(filepath, mode='x', newline='', encoding='utf8')

    def _get_def(self, value: Any, default: Any) -> Any:
        return default if value is None else value

    def get(self, opt: str, default: Any=None) -> Any:
        if (value := self._opt.get(opt, default)) is None:
            return default

        return value

    @abstractmethod
    def write(self, filepath: str, container: GeoContainer):
        raise NotImplementedError()
