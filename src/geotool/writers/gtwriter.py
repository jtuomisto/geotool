from geotool.geometries import BaseGeometry, GeoPoint, GeoContainer
from .geowriter import GeoWriter


class GTWriter(GeoWriter):
    def _clamp(self, value, width: int):
        # TODO: Should warn if value is longer than width and some of it gets cut off

        if isinstance(value, float):
            value = f'{value:.3f}'
        else:
            value = str(value)

        return value[:width].rjust(width)

    def _format(self, g: BaseGeometry) -> str:
        surface = self._get_def(g['surface'], 0)
        line = self._get_def(g['line'], 0)
        code = self._get_def(g['code'], 0)
        point = self._get_def(g['point'], 0)

        return ''.join((
            self._clamp(surface, 8),
            self._clamp(line, 8),
            self._clamp(code, 8),
            self._clamp(point, 8),
            self._clamp(g.north, 14),
            self._clamp(g.east, 14),
            self._clamp(g.elevation, 14),
            '\r\n'
        ))

    def _write_point(self, handle, g: GeoPoint):
        handle.write(self._format(g))

    def write(self, filepath: str, container: GeoContainer):
        container.settings(expand_vertices=True, linear_features=True)

        with self._open(filepath) as handle:
            for g in container.geometries():
                if isinstance(g, GeoPoint):
                    self._write_point(handle, g)
