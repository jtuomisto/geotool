from warnings import warn
from osgeo import ogr
from geotool.schema import GeoLayer
from geotool.geometries import (BaseGeometry, GeoContainer, GeoPoint, GeoLine, GeoPolygon,
                                GeoCircularString, GeoCompoundCurve, GeoCurvePolygon, GeoCollection,
                                GeoMultiPoint, GeoMultiLine, GeoMultiPolygon)
from .gdalbasicwriter import GDALBasicWriter


GEO_TO_OGR = {
    # (3D, 2D)
    GeoPoint: (ogr.wkbPoint25D, ogr.wkbPoint),
    GeoLine: (ogr.wkbLineString25D, ogr.wkbLineString),
    GeoPolygon: (ogr.wkbPolygon25D, ogr.wkbPolygon),
    GeoMultiPoint: (ogr.wkbMultiPoint25D, ogr.wkbMultiPoint),
    GeoMultiLine: (ogr.wkbMultiLineString25D, ogr.wkbMultiLineString),
    GeoMultiPolygon: (ogr.wkbMultiPolygon25D, ogr.wkbMultiPolygon),
    GeoCollection: (ogr.wkbGeometryCollection25D, ogr.wkbGeometryCollection),
    GeoCircularString: (ogr.wkbCircularStringZ, ogr.wkbCircularString),
    GeoCompoundCurve: (ogr.wkbCompoundCurveZ, ogr.wkbCompoundCurve),
    GeoCurvePolygon: (ogr.wkbCurvePolygonZ, ogr.wkbCurvePolygon)
}


class GDALLayerWriter(GDALBasicWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._layers = {}
        self._layer_map = self.get('layer-map', {})

    def _get_layer(self, g: BaseGeometry) -> ogr.Layer:
        schema_layer = self._schema.layer_for(g)

        if (layer := self._layers.get(schema_layer.name, False)) is not False:
            return layer

        layer = self._create_feature_layer(g, schema_layer)
        self._layers[schema_layer.name] = layer

        return layer

    def _create_feature_layer(self, g: BaseGeometry, schema_layer: GeoLayer) -> ogr.Layer:
        layer_name = self._layer_map.get(schema_layer.name, schema_layer.name)
        geom_type = GEO_TO_OGR[schema_layer.geometry_type][int(schema_layer.is_2D)]
        layer = self._create_layer(layer_name, self._srs, geom_type, schema_layer)

        if layer is None:
            return None

        for a in schema_layer.attributes():
            self._create_field(layer, a)

        return layer

    def _write_single(self, g: BaseGeometry):
        layer = self._get_layer(g)

        if layer is None:
            warn(f'Skipping geometry with layer "{g.layer}", layer does not exist', RuntimeWarning)
        else:
            self._create_feature(g, layer)

    def write(self, filepath: str, container: GeoContainer):
        self._init_write(filepath, container)

        self._cont_settings.update({'homogeneous_layers': True})
        container.settings(**self._cont_settings)

        try:
            for g in container.geometries():
                self._write_single(g)
        finally:
            self._close_write()
