from .gdallayerwriter import GDALLayerWriter


class ShapefileWriter(GDALLayerWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._driver_name = 'esri shapefile'
