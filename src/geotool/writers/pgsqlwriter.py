from .postgres import Postgres


class PGSQLWriter(Postgres):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._driver_name = 'pgdump'

        drop = self.get('pgsql-drop', 'if_exists').upper()
        create = 'NO' if self.get('pgsql-no-create', False) else 'YES'

        self._layer_opts.extend([f'CREATE_SCHEMA={create}',
                                 f'CREATE_TABLE={create}',
                                 f'DROP_TABLE={drop}'])

        self._setup_opts()
