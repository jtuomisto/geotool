import re
from osgeo import gdal
from geotool.schema import GeoLayer
from geotool.tools import string_to_ascii
from .gdallayerwriter import GDALLayerWriter


class Postgres(GDALLayerWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        schema = self.get('pgsql-schema', 'public')
        spi_method = self.get('pgsql-spatial', 'gist').upper()
        pg_copy = 'NO' if self.get('pgsql-insert', False) else 'YES'

        self._layer_opts = ['PRECISION=NO',
                            'EXTRACT_SCHEMA_FROM_LAYER_NAME=OFF',
                            f'SCHEMA={schema}',
                            f'SPATIAL_INDEX={spi_method}']

        self._pg_copy = gdal.GetConfigOption('PG_USE_COPY', 'NO')
        gdal.SetConfigOption('PG_USE_COPY', pg_copy)

        rgx = re.compile('[^a-z0-9_]')
        self._name_cleanup = lambda name: rgx.sub('_', string_to_ascii(name.strip().lower()))

    def __del__(self):
        gdal.SetConfigOption('PG_USE_COPY', self._pg_copy)

    def _setup_opts(self):
        self._layer_2d = self._layer_opts + ['DIM=2']
        self._layer_3d = self._layer_opts + ['DIM=3']

    def _layer_opts_for(self, schema_layer: GeoLayer) -> list[str]:
        if schema_layer.is_2D:
            return self._layer_2d

        return self._layer_3d
