import re
from .gdallayerwriter import GDALLayerWriter


class GeoPackageWriter(GDALLayerWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._driver_name = 'gpkg'
        self._dataset_opts = ['DATETIME_FORMAT=UTC']
        self._layer_opts = ['PRECISION=NO']

        rgx = re.compile('[^\\w\\d_ ]')
        self._name_cleanup = lambda name: rgx.sub('_', name.strip())
