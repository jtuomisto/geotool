from .gdalbasicwriter import GDALBasicWriter


class GeoJSONWriter(GDALBasicWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._driver_name = 'geojson'
        self._layer_opts = ['RFC7946=NO', 'WRITE_NON_FINITE_VALUES=NO']
        self._cont_settings = {'linear_features': True}
