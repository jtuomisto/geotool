from warnings import warn
from osgeo import gdal, ogr, osr
from geotool.geometries import BaseGeometry, GeoContainer
from geotool.schema import GeoAttribute, GeoLayer
from geotool.schema.types import SchemaInt32, SchemaOGRDateTime, SchemaFloat
from geotool.tools import Counter
from .geowriter import GeoWriter


COMMIT_N = 5000

NATIVE_TO_FIELD_TYPE = {
    int: (ogr.OFTInteger64, None),
    float: (ogr.OFTReal, None),
    str: (ogr.OFTString, None),
    bool: (ogr.OFTInteger, ogr.OFSTBoolean),
    SchemaInt32: (ogr.OFTInteger, None),
    SchemaOGRDateTime: (ogr.OFTDateTime, None),
    SchemaFloat: (ogr.OFTReal, None)
    # TODO: Maybe implement these
    # ogr.OFTInteger64List
    # ogr.OFTRealList
    # ogr.OFTStringList
    # ogr.OFTWideString
    # ogr.OFTWideStringList
    # ogr.OFTBinary
}


class GDALBasicWriter(GeoWriter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._driver_name = None
        self._transactions = False
        self._commit_counter = Counter(1)
        self._driver = None
        self._dataset = None
        self._dataset_opts = []
        self._layer_opts = []
        self._srs = None
        self._schema = None
        self._name_cleanup = None
        self._cont_settings = {}

    def _layer_opts_for(self, schema_layer: GeoLayer) -> list[str]:
        return self._layer_opts

    def _do_cleanup(self, name: str) -> str:
        if self._name_cleanup is None:
            return name

        cleaned = self._name_cleanup(name)

        if cleaned == name:
            return name

        cleaned_uniq = cleaned
        counter = Counter(0)

        while self._schema.get_layer(cleaned_uniq) is not None:
            cleaned_uniq = f'{cleaned}_{counter()}'

        return cleaned_uniq

    def _create_layer(self, name: str, srs: osr.SpatialReference=None,
                      geom_type=None, schema_layer: GeoLayer=None) -> ogr.Layer:
        if schema_layer is None:
            layer_options = self._layer_opts
        else:
            layer_options = self._layer_opts_for(schema_layer)

        def try_create(**kwargs) -> ogr.Layer:
            try:
                layer = self._dataset.CreateLayer(**kwargs, options=layer_options)
            except RuntimeError as e:
                warn(f'Tried to create layer "{kwargs["name"]}" and failed: {e!s}', RuntimeWarning)
                return None
            else:
                return layer

        layer = None
        lname = self._do_cleanup(name)

        # gdal.Driver has GetMetadata, maybe support can be checked with that
        if geom_type is not None:
            if srs is not None:
                layer = try_create(name=lname, srs=srs, geom_type=geom_type)
            else:
                layer = try_create(name=lname, geom_type=geom_type)

        if layer is None and srs is not None:
            layer = try_create(name=lname, srs=srs)

        if layer is None:
            layer = try_create(name=lname)

        if layer is None:
            warn(f'Unable to create layer "{name}"', RuntimeWarning)

        return layer

    def _create_field(self, layer: ogr.Layer, attr: GeoAttribute):
        if attr.temporary:
            return

        if (fid := layer.FindFieldIndex(attr.name, False)) != -1:
            attr.set_meta('out_field_index', fid)
            return

        field_type = NATIVE_TO_FIELD_TYPE.get(attr.var_type)

        if field_type is None:
            warn(f'Unable to create field "{attr.name}" in layer "{layer.GetName()}"', RuntimeWarning)
            return

        main_type, sub_type = field_type
        field = ogr.FieldDefn(attr.name, main_type)

        if sub_type is not None:
            field.SetSubType(sub_type)

        attr.set_meta('out_field_index', layer.GetLayerDefn().GetFieldCount())
        layer.CreateField(field)

    def _create_feature(self, g: BaseGeometry, layer: ogr.Layer):
        schema_layer = self._schema.layer_for(g)

        if schema_layer.is_2D:
            g.flatten()

        feature = ogr.Feature(layer.GetLayerDefn())
        feature.SetGeometry(g.geometry)

        for attr in schema_layer.attributes():
            if (fid := attr.get_meta('out_field_index')) is None:
                continue

            if attr.var_type is SchemaOGRDateTime:
                feature.SetField(fid, *attr.value_for(g))
            else:
                feature.SetField(fid, attr.value_for(g))

        layer.CreateFeature(feature)

        if self._transactions and self._commit_counter() >= COMMIT_N:
            self._dataset.CommitTransaction()
            self._dataset.StartTransaction()
            self._commit_counter.reset(0)

    def _post_init(self):
        return

    def _init_write(self, filepath: str, container: GeoContainer):
        self._driver = gdal.GetDriverByName(self._driver_name)
        self._dataset = self._driver.Create(filepath, 0, 0, 0, options=self._dataset_opts)
        self._srs = container.crs
        self._schema = container.schema

        self._post_init()

        try:
            self._dataset.SetSpatialRef(self._srs)
        except RuntimeError:
            pass

        self._transactions = self._dataset.TestCapability('ODsCTransactions')

        if self._transactions:
            self._dataset.StartTransaction()

    def _close_write(self):
        if self._transactions:
            self._dataset.CommitTransaction()

        self._dataset = None
        self._driver = None

    def write(self, filepath: str, container: GeoContainer):
        self._init_write(filepath, container)
        layer = self._create_layer('features', self._srs)

        if layer is None:
            raise RuntimeError('Failed to create layer for features')

        for a in self._schema.all_attributes():
            self._create_field(layer, a)

        container.settings(**self._cont_settings)

        try:
            for g in container.geometries():
                self._create_feature(g, layer)
        finally:
            self._close_write()
