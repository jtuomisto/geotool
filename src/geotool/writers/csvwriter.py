import csv
from geotool.geometries import GeoPoint, GeoContainer
from .geowriter import GeoWriter


class CSVWriter(GeoWriter):
    def _write_point(self, w, g: GeoPoint):
        surface = self._get_def(g['surface'], 0)
        line = self._get_def(g['line'], 0)
        code = self._get_def(g['code'], 0)
        point = self._get_def(g['point'], 0)

        w.writerow((
            surface, line, code, point,
            g.north, g.east, g.elevation
        ))

    def write(self, filepath: str, container: GeoContainer):
        with self._open(filepath) as handle:
            writer = csv.writer(handle)

            container.settings(expand_vertices=True, linear_features=True)

            for g in container.geometries():
                if isinstance(g, GeoPoint):
                    self._write_point(writer, g)
