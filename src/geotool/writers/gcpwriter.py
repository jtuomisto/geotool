import re
from geotool.geometries import GeoPoint
from .csvwriter import CSVWriter


class GCPWriter(CSVWriter):
    def _write_point(self, w, g: GeoPoint):
        point = self._get_def(g['point'], 0)
        point = re.sub(r'\s+|,', '_', str(point))
        w.writerow((point, g.east, g.north, g.elevation))
