from geotool.geometries import GeoContainer
from geotool.schema import GeoSchema
from geotool.misc import GeotoolProgress, renumber_feature, unique_point
from geotool.log import print_msg
from geotool.parsers import CSVParser, GTParser, GDALParser
from geotool.writers import (CSVWriter, GTWriter, GeoJSONWriter, GeoPackageWriter,
                             PGSQLWriter, GCPWriter, ShapefileWriter)
from geotool.errors import InvalidArgumentsError


def read_input(input_files: list[str], args) -> GeoContainer:
    parsers = {
        'csv': CSVParser,
        'gt': GTParser,
        'xyz': GTParser,
        'geojson': GDALParser,
        'json': GDALParser,
        'gpkg': GDALParser,
        'shp': GDALParser
    }
    parser_opts = {
        'crs': args.from_crs,
        'encoding': args.encoding,
        'no-lines': args.no_lines,
        'code-layers': args.code_layers,
        'gt-comment-hacks': args.gt_comment_hacks,
        'gt-code-as-line': args.gt_code_as_line,
        'csv-columns': args.csv_columns
    }

    if not isinstance(input_files, list):
        input_files = [input_files]

    if len(input_files) == 0:
        raise InvalidArgumentsError('No files to read')

    container = GeoContainer(GeoSchema(), None, args.to_crs)

    for filepath in input_files:
        _, _, suffix = filepath.lower().rpartition('.')
        # Try to open unknown files with GDALParser
        parser = parsers.get(suffix, GDALParser)
        geoparser = parser(parser_opts)
        cont = geoparser.parse(filepath)
        container.combine(cont)

    return container


def write_output(container: GeoContainer, args):
    writers = {
        'csv': (CSVWriter, '.csv'),
        'gt': (GTWriter, '.gt'),
        'geojson': (GeoJSONWriter, '.geojson'),
        'gpkg': (GeoPackageWriter, '.gpkg'),
        'pgsql': (PGSQLWriter, '.sql'),
        'gcp': (GCPWriter, '.csv'),
        'shape': (ShapefileWriter, None)
    }
    writer_opts = {
        'pgsql-schema': args.pgsql_schema,
        'pgsql-spatial': args.pgsql_spatial,
        'pgsql-insert': args.pgsql_insert,
        'pgsql-no-create': args.pgsql_no_create,
        'pgsql-drop': args.pgsql_drop,
        'layer-map': args.layer_map
    }
    output_file = args.output
    output_type = args.output_type
    writer_cls, suffix = writers[output_type]
    prg = None

    if suffix is not None and not output_file.lower().endswith(suffix):
        output_file += suffix

    if args.renumber:
        container.map_function(renumber_feature)
    elif args.unique:
        container.map_function(unique_point)

    container.settings(expand_vertices=args.write_vertex, linear_features=args.linear)

    print_msg('Writing output to {}, with crs {}', output_file, args.to_crs.GetName())

    if not args.no_progress:
        if container.is_final:
            desc = 'Writing geometries'
        else:
            desc = 'Processing geometries'

        prg = GeotoolProgress(desc)
        container.set_progress_func(prg.setup())

    try:
        writer_cls(writer_opts).write(output_file, container)
    except FileExistsError as e:
        raise RuntimeError(f'File already exists: {output_file}') from e
    except IOError as e:
        raise RuntimeError(f'Writing failed for {output_file}') from e
    finally:
        if prg is not None:
            prg.finish()
