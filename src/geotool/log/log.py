from rich.text import Text
from rich.console import Console
from rich.theme import Theme


THEME = Theme({
    'progress.elapsed': 'white',
    'bar.complete': 'white',
    'bar.pulse': 'white'
})

CON_STDOUT = Console(theme=THEME)
CON_STDERR = Console(theme=THEME, stderr=True)

ERR_STYLE = 'bold red'
WARN_STYLE = 'bold yellow'
MSG_STYLE = 'green'
INFO_STYLE = 'white'


def print_err(frmt: str, *args):
    CON_STDERR.print(Text(f'!! {frmt.format(*args)}', style=ERR_STYLE))


def print_warn(frmt: str, *args):
    CON_STDOUT.print(Text(f'## {frmt.format(*args)}', style=WARN_STYLE))


def print_msg(frmt: str, *args):
    CON_STDOUT.print(Text(f'>> {frmt.format(*args)}', style=MSG_STYLE))


def print_info(frmt: str, *args):
    CON_STDOUT.print(Text(f':: {frmt.format(*args)}', style=INFO_STYLE))
