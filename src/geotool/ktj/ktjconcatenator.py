from zipfile import ZipFile
from pathlib import Path
from typing import Callable, Iterable, Iterator
from os.path import basename
import lxml.etree as XML
from geotool.errors import ParserError
from .lxml_misc import lxml_misc_get_child_text, lxml_misc_get_tag


def _reg_unit_getter(element: XML.Element) -> str:
    return lxml_misc_get_child_text(element, 'kiinteistotunnus')

def _priv_unit_getter(element: XML.Element) -> str:
    return lxml_misc_get_child_text(element, 'kayttooikeusyksikkotunnus')

def _id_attrib_getter(element: XML.Element) -> str:
    return element.attrib.get('{http://www.opengis.net/gml}id')


class KTJConcatenator:
    def __init__(self, directory: str, output_file: str, convert_to_mm: bool=False):
        self._unique_ids = set()
        self._input_dir = directory
        self._output_file = output_file
        self._convert_to_mm = bool(convert_to_mm)
        self._progress = lambda extra_info=None: None

        if not self._output_file.endswith('.xml'):
            self._output_file += '.xml'

    def _read_zip(self, filename: str) -> XML.ElementTree:
        with ZipFile(filename, 'r') as zipobj:
            for name in zipobj.namelist():
                if name.strip().lower().endswith('.meta.xml'):
                    continue

                with zipobj.open(name, 'r') as handle:
                    self._progress(extra_info=name)
                    return XML.parse(handle)

    def _find_default(self, root: XML.ElementTree, tag_name: str) -> XML.Element:
        el = root.find(f'{{*}}{tag_name}')

        if el is None:
            el = XML.SubElement(root, tag_name)

        return el

    def _fix_border_marker_numbers(self, children: Iterable[XML.Element]) -> Iterator[XML.Element]:
        for e in children:
            number = e.find('{*}numero')

            if number is None:
                marker_type = e.find('{*}rajamerkkilaji')

                if marker_type is None:
                    raise ParserError('Invalid xml: missing <rajamerkkilaji>')

                number = XML.Element('numero')
                number.text = '0'
                marker_type.addnext(number)

            if self._convert_to_mm:
                precision = e.find('{*}tasosijaintitarkkuus')

                if precision is not None:
                    pvalue = int(float(lxml_misc_get_child_text(precision)) * 1000)
                    precision.text = str(pvalue)

            yield e

    def _get_children(self, parent: XML.Element) -> Iterator[XML.Element]:
        parent_tag = lxml_misc_get_tag(parent)
        id_getter = None
        children = parent.findall('*')

        match parent_tag:
            case 'rekisteriyksikot':
                id_getter = _reg_unit_getter
            case 'kayttooikeusyksikot':
                id_getter = _priv_unit_getter
            case 'kiinteistorajat' | 'rajamerkit':
                id_getter = _id_attrib_getter

        if parent_tag == 'rajamerkit':
            children = self._fix_border_marker_numbers(children)

        if id_getter is None:
            yield from children
            return

        for e in children:
            e_id = id_getter(e)

            if e_id in self._unique_ids:
                continue

            if e_id is not None:
                self._unique_ids.add(e_id)

            yield e

    def set_progress_func(self, func: Callable[[str, str, int], None]):
        self._progress = func

    def process(self):
        output_xml = None
        reg_units = None
        prop_borders = None
        priv_units = None
        border_markers = None

        for zfile in Path(self._input_dir).iterdir():
            if zfile.is_dir() or not zfile.name.lower().endswith('.zip'):
                continue

            parsed = self._read_zip(zfile)

            if parsed is None:
                raise ParserError('Invalid file:', str(zfile))

            root = parsed.getroot()
            meta = root.find('.//{*}metatiedot')

            if meta is not None:
                root.remove(meta)

            if output_xml is None:
                output_xml = parsed
                reg_units = self._find_default(root, 'rekisteriyksikot')
                prop_borders = self._find_default(root, 'kiinteistorajat')
                priv_units = self._find_default(root, 'kayttooikeusyksikot')
                border_markers = self._find_default(root, 'rajamerkit')

                for e in self._fix_border_marker_numbers(border_markers):
                    # Iterate over the generator,
                    # otherwise nothing is actually done
                    pass

                continue

            for e in root:
                children = self._get_children(e)

                match lxml_misc_get_tag(e):
                    case 'rekisteriyksikot':
                        reg_units.extend(children)
                    case 'kiinteistorajat':
                        prop_borders.extend(children)
                    case 'kayttooikeusyksikot':
                        priv_units.extend(children)
                    case 'rajamerkit':
                        border_markers.extend(children)

        if output_xml is not None:
            self._progress(extra_info=basename(self._output_file))
            output_xml.write(self._output_file)
