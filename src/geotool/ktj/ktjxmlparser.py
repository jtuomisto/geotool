from itertools import chain
from warnings import warn
from typing import Iterator
import lxml.etree as XML
from geotool.tools import get_global_crs
from geotool.schema import GeoSchema, GeoLayer, GeoAttribute
from geotool.schema.types import SchemaInt32, SchemaFloat
from geotool.errors import ParserError
from geotool.geometries import (BaseGeometry, GeoPoint, GeoLine, GeoPolygon,
                                GeoCompoundCurve, GeoCircularString)
from .lxml_misc import lxml_misc_get_child_text, lxml_misc_get_tag


def build_schema() -> GeoSchema:
    kayt_oik_laj = GeoAttribute('kayttooikeusyksikkolaji', SchemaInt32)
    kayt_oik_tun = GeoAttribute('kayttooikeusyksikkotunnus', str, unique=True)
    kayt_oik_num = GeoAttribute('kayttooikeusyksikonOsanNumero', SchemaInt32)
    kayt_tark_sel = GeoAttribute('kayttotarkoitusSelvakielisena', str)
    kayt_tark_lyh = GeoAttribute('kayttotarkoituslyhenne', str)
    kiint_raja_id = GeoAttribute('raja_id', str, unique=True)
    rajamerkki_id = GeoAttribute('rajamerkki_id', str, unique=True)
    palsta_id = GeoAttribute('palsta_id', str, unique=True)
    kiint_raja_laji = GeoAttribute('kiinteistorajalaji', SchemaInt32)
    kiint_tun = GeoAttribute('kiinteistotunnus', str, unique=True)
    lahde = GeoAttribute('lahdeaineisto', SchemaInt32)
    luonne1 = GeoAttribute('luonne1', SchemaInt32)
    luonne2 = GeoAttribute('luonne2', SchemaInt32)
    maa_pint = GeoAttribute('maapintaala', float)
    manttaali = GeoAttribute('manttaali', SchemaFloat)
    nimi = GeoAttribute('nimi', str)
    numero = GeoAttribute('numero', SchemaInt32)
    olem_olo = GeoAttribute('olemassaolo', SchemaInt32)
    olotila = GeoAttribute('olotila', SchemaInt32)
    osaluku = GeoAttribute('osaluku', SchemaFloat)
    raja_merk_laji = GeoAttribute('rajamerkkilaji', SchemaInt32)
    rakenne = GeoAttribute('rakenne', SchemaInt32)
    rek_yks_laji = GeoAttribute('rekisteriyksikkolaji', SchemaInt32)
    rek_pvm = GeoAttribute('rekisterointipvm', str)
    suhd_maa = GeoAttribute('suhdeMaanpintaan', SchemaInt32)
    suhd_perus = GeoAttribute('suhdePeruskiinteistoon', SchemaInt32)
    taso_sij_tark = GeoAttribute('tasosijaintitarkkuus', float)
    teksti = GeoAttribute('tekstiKartalla', str)
    vesi_pint = GeoAttribute('vesipintaala', float)
    arkistoviite = GeoAttribute('arkistoviite', str)

    rek_yks_layer = GeoLayer('Rekisteriyksiköt', GeoPolygon, is_2D=True)
    kayt_oik_yks_poly_layer = GeoLayer('Käyttöoikeusyksiköt_polygon', GeoPolygon, is_2D=True)
    kayt_oik_yks_line_layer = GeoLayer('Käyttöoikeusyksiköt_line', GeoLine, is_2D=True)
    kayt_oik_yks_point_layer = GeoLayer('Käyttöoikeusyksiköt_point', GeoPoint, is_2D=True)
    kiint_raja_layer = GeoLayer('Kiinteistörajat', GeoCompoundCurve, is_2D=True)
    raja_merk_layer = GeoLayer('Rajamerkit', GeoPoint, is_2D=True)

    rek_yks_layer.add_attribute(kiint_tun, palsta_id, olotila, rek_yks_laji,
                                suhd_perus, rek_pvm, nimi, osaluku, manttaali,
                                maa_pint, vesi_pint, kayt_tark_lyh, kayt_tark_sel,
                                arkistoviite, teksti)

    kayt_oik_yks_poly_layer.add_attribute(kayt_oik_tun, olotila, kayt_oik_laj,
                                          rek_pvm, nimi, kayt_oik_num, teksti)

    kayt_oik_yks_line_layer.add_attribute(kayt_oik_tun, olotila, kayt_oik_laj,
                                          rek_pvm, nimi, kayt_oik_num, teksti)

    kayt_oik_yks_point_layer.add_attribute(kayt_oik_tun, olotila, kayt_oik_laj,
                                           rek_pvm, nimi, kayt_oik_num, teksti)

    kiint_raja_layer.add_attribute(kiint_raja_id, kiint_raja_laji,
                                   lahde, luonne1, luonne2, rek_pvm)

    raja_merk_layer.add_attribute(rajamerkki_id, raja_merk_laji, numero, taso_sij_tark,
                                  rakenne, lahde, suhd_maa, olem_olo, rek_pvm)

    schema = GeoSchema()
    schema.add_layer(rek_yks_layer, kayt_oik_yks_poly_layer, kayt_oik_yks_line_layer,
                     kayt_oik_yks_point_layer, kiint_raja_layer, raja_merk_layer)

    return schema


SCHEMA = build_schema()


class KTJXMLParser:
    def __init__(self):
        self._crs = None
        self._lang_attr = '{http://www.w3.org/XML/1998/namespace}lang'
        self._id_attr = '{http://www.opengis.net/gml}id'
        self._lang = 'fi'

    def _create_point(self, element: XML.Element) -> GeoPoint:
        coords = lxml_misc_get_child_text(element, 'pos')

        if coords is None:
            raise ParserError('Missing coordinates')

        north, east = coords.split(' ')
        return GeoPoint(north, east, None, self._crs)

    def _create_line(self, element: XML.Element) -> GeoLine:
        coords = lxml_misc_get_child_text(element, 'coordinates')

        if coords is None:
            raise ParserError('Missing coordinates')

        g = GeoLine(self._crs)

        for pair in coords.split(' '):
            north, east = pair.split(',')
            g.append(north, east, None)

        return g

    def _create_area(self, element: XML.Element) -> GeoPolygon:
        g = GeoPolygon(self._crs)
        exterior = element.iterfind('{*}exterior')
        interior = element.iterfind('{*}interior')

        for elem in chain(exterior, interior):
            for e in elem:
                if e.tag.endswith('LinearRing'):
                    g.append(self._create_line(e))
                else:
                    warn(f'Unhandled ring "{lxml_misc_get_tag(e)}"', RuntimeWarning)

        return g

    def _create_arc(self, element: XML.Element) -> GeoCircularString:
        pos = element.findall('{*}pos')
        points = []

        for p in pos:
            coords = lxml_misc_get_child_text(p)

            if coords is None:
                raise ParserError('Missing coordinates')

            north, east = coords.split(' ')
            points.append(GeoPoint(north, east, None, self._crs))

        g = GeoCircularString.from_two_points_center(*points)
        g.set_crs(self._crs)

        return g

    def _get_geometries(self, element: XML.Element, attrs: dict=None, layer: str=None, linear: bool=False) -> Iterator[BaseGeometry]:
        loc = element.find('{*}sijainti')

        if loc is None:
            return

        for e in loc:
            match lxml_misc_get_tag(e):
                case 'Piste':
                    g = self._create_point(e)
                case 'Alue':
                    g = self._create_area(e)
                case 'Ympyrankaari':
                    g = GeoCompoundCurve(self._crs)
                    g.append(self._create_arc(e))
                case 'Murtoviiva' | 'Kayraviiva':
                    g = GeoCompoundCurve(self._crs)
                    g.append(self._create_line(e))
                case _:
                    continue

            if attrs is not None:
                g.update_attributes(attrs)

            if layer is not None:
                g.set_layer(layer)

            if linear and isinstance(g, GeoCompoundCurve):
                yield g.to_linear()
            else:
                yield g

    def _get_id_attr(self, element: XML.Element) -> str:
        if not element.attrib.has_key(self._id_attr):
            return None

        _, _, e_id = element.attrib.get(self._id_attr, '').rpartition('-')

        if e_id is None or not e_id.isnumeric():
            return None

        return e_id

    def _get_attrs(self, element: XML.Element, attr_names: set, old_attrs: dict=None) -> dict:
        attrs = {} if old_attrs is None else old_attrs

        for e in element:
            tag = lxml_misc_get_tag(e)
            lang = e.get(self._lang_attr)

            if tag not in attr_names:
                continue

            if lang is not None and lang.lower() != self._lang:
                continue

            attrs[tag] = lxml_misc_get_child_text(e)

        return attrs

    def _get_register_unit_archives(self, element: XML.Element) -> str:
        archives = element.find('{*}arkistoviittaus')

        if archives is None:
            return None

        arch_items = archives.findall('.//{*}arkistoviite')

        if len(arch_items) > 0:
            return ', '.join([lxml_misc_get_child_text(a) for a in arch_items])

        return None

    def _fix_border_marker_attrs(self, attrs: dict):
        num = attrs.get('numero')

        if num is None or len(num) == 0:
            return

        num = num.strip().lower()

        if num.isnumeric():
            return

        numbers = ''.join([x for x in filter(lambda x: x.isnumeric(), num)])

        if num.startswith('rp'):
            attrs['rajamerkkilaji'] = '609'

        if len(numbers) > 0:
            attrs['numero'] = numbers
        else:
            attrs['numero'] = None

    def _register_units(self, element: XML.Element) -> Iterator[BaseGeometry]:
        attr_names = SCHEMA.get_layer('Rekisteriyksiköt').attribute_names

        for e in element:
            if not e.tag.endswith('Rekisteriyksikko'):
                continue

            regions = e.find('{*}palstat')

            if regions is None:
                continue

            attrs = self._get_attrs(e, attr_names)

            if (arch := self._get_register_unit_archives(e)) is not None:
                attrs['arkistoviite'] = arch

            for r in regions:
                attrs = self._get_attrs(r, attr_names, attrs)

                if (id_value := self._get_id_attr(r)) is not None:
                    attrs['palsta_id'] = id_value

                for g in self._get_geometries(r, attrs, 'Rekisteriyksiköt'):
                    if isinstance(g, GeoPolygon):
                        yield g

    def _property_borders(self, element: XML.Element) -> Iterator[BaseGeometry]:
        attr_names = SCHEMA.get_layer('Kiinteistörajat').attribute_names

        for e in element:
            if not e.tag.endswith('Kiinteistoraja'):
                continue

            attrs = self._get_attrs(e, attr_names)

            if (id_value := self._get_id_attr(e)) is not None:
                attrs['raja_id'] = id_value

            yield from self._get_geometries(e, attrs, 'Kiinteistörajat')

    def _privilege_units(self, element: XML.Element) -> Iterator[BaseGeometry]:
        geom_layers = {
            GeoPolygon: 'Käyttöoikeusyksiköt_polygon',
            GeoLine: 'Käyttöoikeusyksiköt_line',
            GeoPoint: 'Käyttöoikeusyksiköt_point'
        }

        for e in element:
            if not e.tag.endswith('Kayttooikeusyksikko'):
                continue

            unit_parts = e.find('{*}kayttooikeusyksikonOsat')

            if unit_parts is None:
                continue

            for u in unit_parts:
                for g in self._get_geometries(u, linear=True):
                    layer = geom_layers.get(g.__class__)
                    attr_names  = SCHEMA.get_layer(layer).attribute_names
                    attrs = self._get_attrs(e, attr_names)
                    g.set_layer(layer)
                    g.update_attributes(self._get_attrs(u, attr_names, attrs))
                    yield g

    def _border_markers(self, element: XML.Element) -> Iterator[BaseGeometry]:
        attr_names = SCHEMA.get_layer('Rajamerkit').attribute_names

        for e in element:
            if not e.tag.endswith('Rajamerkki'):
                continue

            attrs = self._get_attrs(e, attr_names)

            if (id_value := self._get_id_attr(e)) is not None:
                attrs['rajamerkki_id'] = id_value

            self._fix_border_marker_attrs(attrs)
            yield from self._get_geometries(e, attrs, 'Rajamerkit')

    def set_crs(self, crs: str):
        self._crs = get_global_crs(crs)

    def parse_meta_xml(self, fileobj):
        root = XML.parse(fileobj).getroot()
        srsName = root.find('.//{*}srsName')
        crs = lxml_misc_get_child_text(srsName)

        if crs is None:
            return

        self.set_crs(crs)

    def parse_xml(self, fileobj) -> Iterator[BaseGeometry]:
        if self._crs is None:
            raise ValueError('Missing CRS information')

        root = XML.parse(fileobj).getroot()

        for e in root:
            match lxml_misc_get_tag(e):
                case 'rekisteriyksikot':
                    yield from self._register_units(e)
                case 'kiinteistorajat':
                    yield from self._property_borders(e)
                case 'kayttooikeusyksikot':
                    yield from self._privilege_units(e)
                case 'rajamerkit':
                    yield from self._border_markers(e)
