import os.path
import re
from warnings import warn
from datetime import datetime
from urllib.parse import urljoin
from typing import Iterator, Callable
import requests
from bs4 import BeautifulSoup


URL = 'https://ws.nls.fi/aineistopalvelu/{ktj_id}/ktjaineistoluovutus/'
DATE_FORMAT = '%a, %d %b %Y %H:%M:%S %Z'


class KTJDownloader:
    def __init__(self, directory: str, ktj_id: str, auth: tuple[str, str]):
        user, password = auth

        if user is None or password is None:
            raise TypeError('User and password must be provided to access KTJ')

        if ktj_id is None:
            raise ValueError('KTJ ID must be provided')

        self._dir = directory
        self._url = URL.format(ktj_id=ktj_id)
        self._auth = (user, password)
        self._progress = lambda extra_info=None: None

    def _parse_file_urls(self, html_text: str) -> Iterator[tuple[str, str]]:
        rgx = re.compile('/(\\d+_ATP.+\\.zip)$')
        parser = BeautifulSoup(html_text, 'lxml')
        files = {}

        # First tr is a header
        for row in parser.find_all('tr')[1:]:
            href, date = row.contents[0].a.get('href'), row.contents[-1].string
            match = rgx.search(href)

            if match is None:
                continue

            filename = match.group(1)
            code, _, _ = filename.partition('_')
            url = urljoin(self._url, href)

            try:
                updated = datetime.strptime(date, DATE_FORMAT)
            except ValueError:
                warn(f'Skipping file {filename}, can\'t parse date')
                continue

            timestamp = updated.timestamp()

            if code not in files or files[code][2] < timestamp:
                files[code] = (filename, url, timestamp)

        for filename, url, _ in files.values():
            yield filename, url

    def set_progress_func(self, func: Callable[[str, str, int], None]):
        self._progress = func

    def download(self):
        req = requests.get(self._url, auth=self._auth)

        for filename, url in self._parse_file_urls(req.text):
            file_req = requests.get(url, auth=self._auth)
            full_path = os.path.join(self._dir, filename)

            with open(full_path, mode='xb') as handle:
                self._progress(extra_info=filename)

                for data in file_req.iter_content(chunk_size=4096):
                    handle.write(data)
                    self._progress()
