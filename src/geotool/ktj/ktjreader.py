from zipfile import ZipFile
from pathlib import Path
from typing import Iterator
from geotool.geometries import BaseGeometry, GeoContainer
from .ktjxmlparser import KTJXMLParser, SCHEMA


class KTJReader:
    def __init__(self, crs):
        self._container = GeoContainer(SCHEMA, None, crs)

    def _parse_zip(self, filename: str) -> Iterator[BaseGeometry]:
        with ZipFile(filename, 'r') as zipobj:
            parser = KTJXMLParser()
            meta_name = None
            data_names = []

            for name in zipobj.namelist():
                if name.strip().lower().endswith('.meta.xml'):
                    meta_name = name
                else:
                    data_names.append(name)

            with zipobj.open(meta_name, 'r') as handle:
                parser.parse_meta_xml(handle)

            for name in data_names:
                with zipobj.open(name, 'r') as handle:
                    yield from parser.parse_xml(handle)

    def get_container(self) -> GeoContainer:
        return self._container

    def read_zip_directory(self, directory: str):
        for zfile in Path(directory).iterdir():
            if zfile.is_dir() or not zfile.name.lower().endswith('.zip'):
                continue

            cont = GeoContainer(SCHEMA, self._parse_zip(zfile), None)
            self._container.combine(cont)
