import lxml.etree as XML


def lxml_misc_get_tag(element: XML.Element) -> str:
    return XML.QName(element).localname


def lxml_misc_get_child_text(element: XML.Element, child_tag: str=None) -> str:
    el = element

    if child_tag is not None:
        el = element.find(f'.//{{*}}{child_tag}')

    if el is None or el.text is None:
        return None

    text = el.text.strip()

    if len(text) == 0:
        return None

    return text
