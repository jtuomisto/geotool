from geotool.misc import GeotoolProgress
from geotool.ktj import KTJConcatenator
from geotool.log import print_info


def concat_ktj(args):
    print_info('Concatenating ktj-data')

    concatenator = KTJConcatenator(args.directory, args.output, args.millimeters)
    prg = None

    if not args.no_progress:
        prg = GeotoolProgress('Concatenating XML')
        concatenator.set_progress_func(prg.setup())

    concatenator.process()

    if prg is not None:
        prg.finish()
