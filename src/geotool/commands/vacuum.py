import geotool.fileio as io
from geotool.geometries import GeoContainer
from geotool.misc import GeotoolProgress
from geotool.log import print_info


def vacuum(args):
    print_info('Vacuuming data')

    container = io.read_input(args.file, args)
    schema = container.schema
    null_map = {}
    geom = []
    prg = None

    container.settings(remove_duplicates=False)

    if not args.no_progress:
        prg = GeotoolProgress('Reading geometries')
        container.set_progress_func(prg.setup())

    for g in container.geometries():
        layer = schema.layer_for(g)

        for attr in layer.attributes():
            key = (layer.name, attr.name)
            null_map[key] = null_map.get(key, True) and attr.value_for(g) is None

        geom.append(g)

    if prg is not None:
        prg.finish()

    removed_attrs = set()

    for (layer_name, attr_name), is_null in null_map.items():
        if not is_null:
            continue

        schema.get_layer(layer_name).delete_attribute(attr_name)
        removed_attrs.add(attr_name)

    print_info(f'Deleted attributes: {", ".join(removed_attrs)}')

    new_container = GeoContainer(schema, geom, container.crs, geom_is_final=True)
    io.write_output(new_container, args)
