import geotool.fileio as io
from geotool.ktj import KTJReader
from geotool.log import print_info


def read_ktj(args):
    print_info('Parsing ktj-data')

    reader = KTJReader(args.to_crs)
    reader.read_zip_directory(args.directory)
    io.write_output(reader.get_container(), args)
