import geotool.fileio as io
from geotool.log import print_info
from geotool.errors import InvalidArgumentsError


def replace(args):
    print_info('Replacing attribute values')

    repl = {}

    for r in args.change:
        try:
            item, old, new = [x.strip() for x in r.split(':')]
        except ValueError as e:
            raise InvalidArgumentsError('Invalid replacement', r) from e

        repl_list = repl.setdefault(item, [])
        repl_list.append((old, new))

    repl = repl.items()
    container = io.read_input(args.files, args)

    def _replace(g):
        for i, r in repl:
            v = 0 if g[i] is None else g[i]
            t = type(v)

            for (o, n) in r:
                try:
                    ov = t(o)
                    nv = t(n)
                except (ValueError, TypeError):
                    continue

                if v == ov:
                    g[i] = nv

        return g

    container.map_function(_replace)
    io.write_output(container, args)
