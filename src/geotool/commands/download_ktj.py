from geotool.ktj import KTJDownloader
from geotool.misc import needs_auth, GeotoolProgress
from geotool.log import print_info


@needs_auth
def download_ktj(args):
    print_info('Downloading ktj-data')

    downloader = KTJDownloader(args.directory, args.id, args.auth)
    prg = None

    if not args.no_progress:
        prg = GeotoolProgress('Downloading files')
        downloader.set_progress_func(prg.setup())

    downloader.download()

    if prg is not None:
        prg.finish()
