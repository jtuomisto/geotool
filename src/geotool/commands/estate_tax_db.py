from geotool.tax import TaxesRanger
from geotool.misc import GeotoolProgress


def estate_tax_db(args):
    tax = TaxesRanger(args.file)
    outfile = args.output
    prg = None

    if not args.no_progress:
        prg = GeotoolProgress('Processing entries')
        tax.set_progress_func(prg.setup())

    if not outfile.lower().endswith('.db'):
        outfile += '.db'

    tax.write_to_db(outfile)

    if prg is not None:
        prg.finish()
