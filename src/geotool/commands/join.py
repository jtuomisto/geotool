import geotool.fileio as io
from geotool.log import print_info


def join(args):
    print_info('Joining and formatting files')

    container = io.read_input(args.files, args)
    io.write_output(container, args)
