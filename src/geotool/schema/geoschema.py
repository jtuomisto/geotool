from __future__ import annotations
from typing import Any, Iterator, Type
from geotool.tools import Counter
from geotool.errors import SchemaError
from geotool.geometries import (BaseGeometry, BaseCurveGeometry, GeoPoint, GeoLine, GeoPolygon,
                                GeoCircularString, GeoCompoundCurve, GeoCurvePolygon, GeoCollection)
from .types import SchemaBaseType


class GeoAttribute:
    def __init__(self, name: str, attr_type: Type=str, unique: bool=False, temporary: bool=False):
        # attribute types should be hashable
        # unique: value of this attribute should be unique among the parent layer
        # temporary: this attribute should not be written to file
        self._name = name
        self._type = attr_type
        self._unique = unique
        self._temp = temporary
        self._metadata = {}

    @property
    def name(self) -> str:
        return self._name

    @property
    def var_type(self) -> Type:
        return self._type

    @property
    def unique(self) -> bool:
        return self._unique

    @property
    def temporary(self) -> bool:
        return self._temp

    def get_meta(self, name: str, default: Any=None) -> Any:
        return self._metadata.get(name, default)

    def set_meta(self, name: str, value: Any):
        self._metadata[name] = value

    def transfer(self, src: BaseGeometry, dest: BaseGeometry):
        dest[self._name] = self.value_for(src)

    def clone(self) -> GeoAttribute:
        attr = GeoAttribute(self._name, self._type, self._unique, self._temp)
        attr._metadata.update(self._metadata)
        return attr

    def value_for(self, g: BaseGeometry) -> Any:
        return self.try_cast(g[self._name])

    def try_cast(self, value: Any) -> Any:
        if value is None:
            return None

        is_schema_type = issubclass(self._type, SchemaBaseType)

        if isinstance(value, SchemaBaseType):
            return value.real_value()

        if not is_schema_type and self._type is not str and isinstance(value, self._type):
            return value

        try:
            typed_value = self._type(value)
        except (ValueError, TypeError):
            return None

        if self._type is str:
            typed_value = typed_value.strip()

            if len(typed_value) == 0:
                return None
        elif is_schema_type:
            return typed_value.real_value()

        return typed_value

    def combine(self, other: GeoAttribute):
        if self is other:
            return

        if self._type is not other._type:
            self._type = str

        if self._temp != other._temp:
            self._temp = False

        self._unique = other._unique
        self._metadata.update(other._metadata)


class GeoLayer:
    def __init__(self, name: str, geom_type: Type=None, is_2D: bool=False):
        self._name = name
        self._type = geom_type
        self._is_deleted = False
        self._is_2d = is_2D
        self._attributes = {}

    @property
    def name(self) -> str:
        return self._name

    @property
    def is_homogeneous(self) -> bool:
        return self._type is not None

    @property
    def is_deleted(self) -> bool:
        return self._is_deleted

    @property
    def is_2D(self) -> bool:
        return self._is_2d

    @property
    def geometry_type(self) -> Type:
        return self._type

    @property
    def geometry_name(self) -> str:
        if self._type is None:
            return 'None'

        return self._type.__name__

    @property
    def attribute_names(self) -> set[str]:
        return set(self._attributes.keys())

    def delete(self):
        self._is_deleted = True

    def attributes(self) -> Iterator[GeoAttribute]:
        yield from self._attributes.values()

    def unique_attributes(self) -> Iterator[GeoAttribute]:
        for a in self.attributes():
            if a.unique:
                yield a

    def clone(self, name: str=None, geom_type: Type=None) -> GeoLayer:
        if self.is_deleted:
            raise SchemaError('Layer is read-only (deleted)')

        name = self._name if name is None else name
        geom_type = self._type if geom_type is None else geom_type
        gl = GeoLayer(name, geom_type, self._is_2d)
        gl._attributes = self._attributes

        return gl

    def transfer_attributes(self, src: BaseGeometry, dest: BaseGeometry):
        for a in self.attributes():
            a.transfer(src, dest)

    def get_attribute(self, name: str) -> GeoAttribute:
        return self._attributes.get(name)

    def add_attribute(self, *args):
        if self.is_deleted:
            raise SchemaError('Layer is read-only (deleted)')

        for attr in args:
            if attr.name in self._attributes:
                raise SchemaError('Attribute already exists', attr.name)

            self._attributes[attr.name] = attr.clone()

    def delete_attribute(self, name_or_attr: str | GeoAttribute):
        if self.is_deleted:
            raise SchemaError('Layer is read-only (deleted)')

        name = name_or_attr

        if isinstance(name_or_attr, GeoAttribute):
            name = name_or_attr.name

        if name not in self._attributes:
            return

        self._attributes.pop(name)

    def combine(self, other: GeoLayer):
        if self.is_deleted:
            raise SchemaError('Layer is read-only (deleted)')

        if self is other:
            return

        if self._type is not other._type:
            self._type = None

        if self._is_2d != other._is_2d:
            self._is_2d = False

        for a in other.attributes():
            if a.name not in self._attributes:
                self.add_attribute(a)
            else:
                self._attributes[a.name].combine(a)


class GeoSchema:
    def __init__(self):
        self._layers = {}
        self._layer_names = set()
        self._real_layer = {}
        self._linearized = False
        self._homogenized = False
        self._expanded = False

    def _class_to_name(self, gcls: BaseGeometry) -> str:
        return f'{gcls.__name__[3:].lower()}s'

    @property
    def is_linear(self) -> bool:
        if self._linearized:
            return True

        for layer in self.layers():
            geom_type = layer.geometry_type

            if geom_type is None or issubclass(geom_type, BaseCurveGeometry):
                return False

        return True

    @property
    def is_homogeneous(self) -> bool:
        if self._homogenized:
            return True

        for layer in self.layers():
            if not layer.is_homogeneous:
                return False

        return True

    @property
    def is_expanded(self) -> bool:
        return self._expanded

    def all_attributes(self) -> Iterator[GeoAttribute]:
        for layer in self.layers():
            yield from layer.attributes()

    def layers(self) -> Iterator[GeoLayer]:
        for layer in self._layers.values():
            if not layer.is_deleted:
                yield layer

    def unique_layer_name(self, name: str) -> str:
        unique_name = name
        counter = Counter(0)

        while unique_name in self._layer_names:
            unique_name = f'{name}_{counter()}'

        return unique_name

    def get_layer(self, name: str) -> GeoLayer:
        # Should be used only for geometry that doesn't exists yet (i.e. in parsers)
        # Otherwise use layer_for
        return self._layers.get(name)

    def layer_for(self, g: BaseGeometry) -> GeoLayer:
        layer = self._real_layer.get((g.layer, g.__class__))
        return layer if layer is not None else self.get_layer(g.layer)

    def add_layer(self, *args):
        for layer in args:
            if layer.name in self._layers:
                raise SchemaError('Layer already exists', layer.name)

            self._layer_names.add(layer.name)
            self._layers[layer.name] = layer

    def delete_layer(self, name_or_layer: str | GeoLayer):
        name = name_or_layer

        if isinstance(name_or_layer, GeoLayer):
            name = name_or_layer.name

        if name not in self._layers:
            return

        self._layers[name].delete()

    def combine(self, other: GeoSchema):
        if self is other:
            return

        for layer in other.layers():
            if layer.name not in self._layers:
                self.add_layer(layer)
            else:
                self._layers[layer.name].combine(layer)

    def linearize(self):
        if self._linearized:
            return

        self.homogenize()
        self._linearized = True
        new_geom = {GeoCircularString: GeoLine,
                    GeoCompoundCurve: GeoLine,
                    GeoCurvePolygon: GeoPolygon}

        for layer in self.layers():
            geom_type = layer.geometry_type

            if not layer.is_homogeneous or not issubclass(geom_type, BaseCurveGeometry):
                continue

            new_geom_type = new_geom.get(geom_type)
            nlayer = layer.clone(geom_type=new_geom_type)
            self._layers[layer.name] = nlayer
            layer.delete()

    def homogenize(self):
        if self._homogenized:
            return

        self._homogenized = True

        geom_types = (GeoPoint, GeoLine, GeoPolygon, GeoCircularString,
                      GeoCompoundCurve, GeoCurvePolygon)
        layer_names = [(cls, self._class_to_name(cls)) for cls in geom_types]
        add_layers = []

        for layer in self.layers():
            if layer.is_homogeneous and layer.geometry_type is not GeoCollection:
                continue

            for geom, name in layer_names:
                nlayer_name = self.unique_layer_name(f'{layer.name}_{name}')
                nlayer = layer.clone(nlayer_name, geom)
                self._real_layer[(layer.name, geom)] = nlayer
                add_layers.append(nlayer)

            layer.delete()

        for a in add_layers:
            self.add_layer(a)

    def expand(self):
        if self._expanded:
            return

        self.homogenize()
        self._expanded = True
        add_layers = []

        for layer in self.layers():
            if layer.geometry_type is GeoPoint:
                continue

            nlayer_name = self.unique_layer_name(layer.name + '_vertices')
            nlayer = layer.clone(nlayer_name, GeoPoint)
            self._real_layer[(layer.name, GeoPoint)] = nlayer
            add_layers.append(nlayer)

        for a in add_layers:
            self.add_layer(a)
