from __future__ import annotations
from datetime import datetime
from abc import abstractmethod, ABC
from typing import Any


class SchemaBaseType(ABC):
    @abstractmethod
    def real_value(self) -> Any:
        raise NotImplementedError()


class SchemaInt32(SchemaBaseType):
    def __init__(self, value: str | int):
        self.value = int(value)

    def __eq__(self, other: SchemaInt32) -> bool:
        if self is other:
            return True

        if not isinstance(other, SchemaInt32):
            return False

        return self.value == other.value

    def __hash__(self) -> int:
        return self.value

    def real_value(self) -> int:
        return self.value


# Stupid hack to convert strings to float
# Locales are program-wide and pain to use
class SchemaFloat(SchemaBaseType):
    def __init__(self, value: str | float):
        self.value = self._fix(value)

    def _fix(self, v: str | float) -> float:
        if isinstance(v, float):
            return v

        if not isinstance(v, str):
            v = str(v).strip()

        try:
            v = float(v)
        except ValueError:
            pass
        else:
            return v

        try:
            # Turn , into . and remove spaces
            v = float(v.translate({ ord(','): '.', ord(' '): '' }))
        except ValueError as e:
            raise ValueError('Cannot coerce value to float') from e
        else:
            return v

    def __eq__(self, other: SchemaFloat) -> bool:
        if self is other:
            return True

        if not isinstance(other, SchemaFloat):
            return False

        return self.value == other.value

    def __hash__(self) -> int:
        return hash(self.value)

    def real_value(self) -> float:
        return self.value


class SchemaOGRDateTime(SchemaBaseType):
    def __init__(self, dt: list):
        y, m, d, H, M, S, self.tzf = map(int, dt)

        try:
            _ = datetime(y, m, d, H, M, S)
        except (ValueError, OverflowError):
            y, m, d, H, M, S = 1970, 1, 1, 0, 0, 0

        self.y, self.m, self.d, self.H, self.M, self.S, = y, m, d, H, M, S

    def __eq__(self, other: SchemaOGRDateTime) -> bool:
        if self is other:
            return True

        if not isinstance(other, SchemaOGRDateTime):
            return False

        return (self.y == other.y and
                self.m == other.m and
                self.d == other.d and
                self.H == other.H and
                self.M == other.M and
                self.S == other.S and
                self.tzf == other.tzf)

    def __hash__(self) -> int:
        return hash((self.y, self.m, self.d, self.H, self.M, self.S, self.tzf))

    def real_value(self) -> tuple:
        return self.y, self.m, self.d, self.H, self.M, self.S, self.tzf
