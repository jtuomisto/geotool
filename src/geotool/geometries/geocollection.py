from __future__ import annotations
from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry


class GeoCollection(BaseGeometry):
    _geometry_type = ogr.wkbGeometryCollection

    def append(self, geom: BaseGeometry):
        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append geometry to collection', success)
