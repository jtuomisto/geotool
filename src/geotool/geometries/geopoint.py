from __future__ import annotations
from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry


class GeoPoint(BaseGeometry):
    _geometry_type = ogr.wkbPoint

    def __init__(self, north: float=None, east: float=None, elevation: float=None,
                 crs=None, layer: str=None, geometry: ogr.Geometry=None):
        super().__init__(crs, layer, geometry)

        if north is not None and east is not None:
            self.set_coords(north, east, elevation)

    @property
    def north(self) -> float:
        return self._geom.GetY()

    @property
    def east(self) -> float:
        return self._geom.GetX()

    @property
    def elevation(self) -> float:
        return self._geom.GetZ()

    @property
    def as_tuple(self) -> tuple[float, float, float]:
        return self._geom.GetPoint()

    def append(self, *args, **kwargs):
        raise GeometryError('Cannot append to points')

    def set_coords(self, north: float, east: float, elev: float=None):
        self._geom.SetPoint(0, float(east), float(north), 0.0 if elev is None else float(elev))

    def offset(self, x_off: float=0, y_off: float=0, z_off: float=0):
        x, y, z = self.as_tuple
        self._geom.SetPoint(0, x + x_off, y + y_off, z + z_off)

    def distance2D(self, other: GeoPoint) -> float:
        if self is other:
            return 0

        return self._geom.Distance(other._geom)

    def midpoint(self, other: GeoPoint) -> GeoPoint:
        if self is other:
            return self

        east = (self.east + other.east) / 2
        north = (self.north + other.north) / 2
        elev = (self.elevation + other.elevation) / 2

        return GeoPoint(north, east, elev, self._crs, self._layer)
