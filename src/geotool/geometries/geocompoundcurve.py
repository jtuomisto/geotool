from __future__ import annotations
from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .basecurvegeometry import BaseCurveGeometry
from .geoline import GeoLine
from .geocircularstring import GeoCircularString


class GeoCompoundCurve(BaseGeometry, BaseCurveGeometry):
    _geometry_type = ogr.wkbCompoundCurve

    def append(self, geom: GeoLine | GeoCircularString):
        if not isinstance(geom, GeoLine | GeoCircularString):
            raise TypeError('Compound curve accepts only lines and circular strings')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append geometry to compound curve', success)

    def to_linear(self) -> GeoLine:
        linear = self._linearize(self._geom)
        g = GeoLine(geometry=linear)
        g.set_crs(self._crs)
        g.set_layer(self._layer)
        g.update_attributes(self)

        return g
