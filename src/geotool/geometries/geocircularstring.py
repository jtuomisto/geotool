from __future__ import annotations
import math
from osgeo import ogr
from .basegeometry import BaseGeometry
from .basecurvegeometry import BaseCurveGeometry
from .geopoint import GeoPoint
from .geoline import GeoLine


class GeoCircularString(BaseGeometry, BaseCurveGeometry):
    _geometry_type = ogr.wkbCircularString

    def __len__(self) -> int:
        return self._geom.GetPointCount()

    @staticmethod
    def from_two_points_center(start: GeoPoint, center: GeoPoint, end: GeoPoint) -> GeoCircularString:
        g = GeoCircularString()
        g.append(start)

        if start.equals(end):
            g.append(center)
            g.append(start)  # This is not a mistake
            return g

        radius = center.distance2D(end)
        mid = start.midpoint(end)
        mid_angle = math.atan2(mid.north - center.north, mid.east - center.east)
        east = center.east + math.cos(mid_angle) * radius
        north = center.north + math.sin(mid_angle) * radius
        g.append(GeoPoint(north, east, mid.elevation, mid.crs))
        g.append(end)

        return g

    def append(self, point_or_north: GeoPoint | float, east: float=None, elev: float=None):
        if isinstance(point_or_north, GeoPoint):
            point_or_north.set_crs(self._crs)
            self._geom.AddPoint(*point_or_north.as_tuple)
        else:
            self._geom.AddPoint(float(east), float(point_or_north), 0.0 if elev is None else float(elev))

    def to_linear(self) -> GeoLine:
        linear = self._linearize(self._geom)
        g = GeoLine(geometry=linear)
        g.set_crs(self._crs)
        g.set_layer(self._layer)
        g.update_attributes(self)

        return g
