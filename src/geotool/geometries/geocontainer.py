from __future__ import annotations
from itertools import chain
from typing import Iterable, Iterator, Callable
from osgeo import ogr, osr
from geotool.schema import GeoSchema, GeoLayer
from .basegeometry import BaseGeometry
from .basecurvegeometry import BaseCurveGeometry
from .geopoint import GeoPoint
from .geocollection import GeoCollection
from .misc import get_geo_from_ogr


class GeoContainer:
    def __init__(self, schema: GeoSchema, geom: Iterable[BaseGeometry],
                       crs: osr.SpatialReference, geom_is_final: bool=False):
        # geom_is_final: True if geom is a list of BaseGeometry instead of an iterator
        self._schema = schema
        self._geom = None if geom is None else iter(geom)
        self._crs = crs
        self._is_final = geom_is_final
        self._map_funcs = []
        self._unique_attrs = {}
        self._duplicates = {}
        self._exhausted = False
        self._progress = lambda: None
        self._settings = {'remove_duplicates': True,
                          'homogeneous_layers': False,
                          'linear_features': False,
                          'expand_vertices': False}

    def _find_uniques(self):
        for layer in self._schema.layers():
            for attr in layer.unique_attributes():
                uniq_layer = self._unique_attrs.setdefault(layer.name, [])
                uniq_layer.append(attr)

    def _is_duplicate(self, g: BaseGeometry, layer: GeoLayer) -> bool:
        if layer.name not in self._unique_attrs:
            return False

        values_hash = hash(tuple([a.value_for(g) for a in self._unique_attrs[layer.name]]))
        dupl_key = (layer.name, g.__class__, values_hash)
        dupl_list = self._duplicates.setdefault(dupl_key, [])

        for dg in dupl_list:
            if g.equals(dg):
                return True

        dupl_list.append(g)
        return False

    def _mapping(self, it: Iterable, set_crs: bool=True) -> Iterator:
        def map_crs(g):
            g.set_crs(self._crs)
            return g

        if set_crs:
            it = map(map_crs, it)

        for func in self._map_funcs:
            it = map(func, it)

        return it

    def _expand(self, g: BaseGeometry, nodes: Iterable[ogr.Geometry], layer: GeoLayer) -> Iterator[GeoPoint]:
        for n in nodes:
            gp = get_geo_from_ogr(n)
            gp.set_crs(g.crs)
            gp.set_layer(layer.name)
            layer.transfer_attributes(g, gp)
            yield gp

    def _explode(self, g: GeoCollection) -> Iterator[BaseGeometry]:
        for n in g.geoms():
            geo = get_geo_from_ogr(n)
            geo.set_crs(g.crs)
            geo.set_layer(g.layer)
            geo.update_attributes(g)
            yield geo

    def _explode_collections(self, it: Iterable) -> Iterator[BaseGeometry]:
        for g in it:
            if isinstance(g, GeoCollection):
                yield from self._mapping(self._explode(g), False)
            else:
                yield g

    def _linearize_curves(self, it: Iterable) -> Iterator[BaseGeometry]:
        for g in it:
            if isinstance(g, BaseCurveGeometry):
                yield g.to_linear()
            else:
                yield g

    def _iterate(self) -> Iterator[BaseGeometry]:
        mapped = self._mapping(self._geom, not self._is_final)
        rem_dupl = self._settings.get('remove_duplicates')
        expand = self._settings.get('expand_vertices')
        linear = self._settings.get('linear_features')
        homogeneous = self._settings.get('homogeneous_layers')

        if homogeneous or linear:
            mapped = self._explode_collections(mapped)

        if linear:
            mapped = self._linearize_curves(mapped)

        if rem_dupl:
            self._find_uniques()

        check_unique = rem_dupl and len(self._unique_attrs) > 0

        for g in mapped:
            layer = self._schema.layer_for(g)

            if not check_unique or not self._is_duplicate(g, layer):
                yield g
                self._progress()

            if not expand or isinstance(g, GeoPoint):
                continue

            # Expanded vertices do not count as duplicates
            for gex in self._mapping(self._expand(g, g.points(), layer), False):
                yield gex
                self._progress()

    @property
    def crs(self) -> osr.SpatialReference:
        return self._crs

    @property
    def schema(self) -> GeoSchema:
        return self._schema

    @property
    def is_final(self) -> bool:
        return self._is_final

    def settings(self, **kwargs):
        for k, v in kwargs.items():
            if k not in self._settings:
                raise KeyError(f'Invalid setting: {k}')

            self._settings[k] = bool(v)

    def geometries(self) -> Iterator[BaseGeometry]:
        if self._geom is None:
            return

        if self._exhausted:
            raise RuntimeError('Container has been exhausted')

        self._exhausted = True

        if self._settings.get('homogeneous_layers'):
            self._schema.homogenize()

        if self._settings.get('linear_features'):
            self._schema.linearize()

        if self._settings.get('expand_vertices'):
            self._schema.expand()

        yield from self._iterate()

    def set_progress_func(self, func: Callable):
        self._progress = func

    def map_function(self, func: Callable[[BaseGeometry], BaseGeometry]):
        if self._exhausted:
            raise RuntimeError('Container has been exhausted')

        self._map_funcs.append(func)

    def combine(self, other: GeoContainer):
        if self._exhausted:
            raise RuntimeError('Container has been exhausted')

        if self is other:
            return

        self._schema.combine(other._schema)

        if self._geom is None:
            self._geom = other._geom
        elif other._geom is not None:
            self._geom = chain(self._geom, other._geom)

        self._map_funcs.extend(other._map_funcs)
