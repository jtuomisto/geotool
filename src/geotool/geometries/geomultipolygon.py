from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geopolygon import GeoPolygon


class GeoMultiPolygon(BaseGeometry):
    _geometry_type = ogr.wkbMultiPolygon

    def append(self, geom: GeoPolygon):
        if not isinstance(geom, GeoPolygon):
            raise TypeError('Multi-polygon accepts only polygons')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append polygon to multi-polygon', success)
