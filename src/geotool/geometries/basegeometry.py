from __future__ import annotations
from abc import abstractmethod, ABC
from typing import Any, Iterator
from osgeo import ogr, osr
from geotool.errors import GeometryError
from geotool.tools import get_global_crs, get_transformation


class BaseGeometry(ABC):
    _geometry_type = ogr.wkbNone

    def __init__(self, crs=None, layer: str=None, geometry: ogr.Geometry=None):
        if geometry is not None:
            self._geom = self._get_geometry(geometry)
        else:
            self._geom = ogr.Geometry(self._geometry_type)

        self._layer = layer
        self._attrs = {}
        self._crs = None

        if crs is not None:
            self.set_crs(crs)

    def __eq__(self, other: BaseGeometry):
        return self.equals(other)

    def __len__(self) -> int:
        return self._geom.GetGeometryCount()

    def __getitem__(self, key: str) -> Any:
        return self._attrs.get(key)

    def __setitem__(self, key: str, value: Any):
        self._attrs[key] = value

    def __repr__(self) -> str:
        crs = 'None' if self._crs is None else self._crs.GetName()
        layer = 'None' if self._layer is None else self._layer
        return f'<{self.__class__.__name__} (crs: {crs}, layer: {layer})>'

    def _get_geometry(self, geom: ogr.Geometry) -> ogr.Geometry:
        if ogr.GT_IsSubClassOf(geom.GetGeometryType(), self._geometry_type):
            return geom.Clone()

        forced_geom = ogr.ForceTo(geom, self._geometry_type)

        if not ogr.GT_IsSubClassOf(forced_geom.GetGeometryType(), self._geometry_type):
            raise GeometryError('Cannot force geometry to right type')

        return forced_geom

    def _ogr_point(self, coord_tuple: tuple[float, float, float]) -> ogr.Geometry:
        g = ogr.Geometry(ogr.wkbPoint25D)
        g.AddPoint(*coord_tuple)
        g.AssignSpatialReference(self._crs)
        return g

    @property
    def geometry(self) -> ogr.Geometry:
        return self._geom

    @property
    def crs(self) -> osr.SpatialReference:
        return self._crs

    @property
    def layer(self) -> str:
        return self._layer

    @abstractmethod
    def append(self, *args):
        raise NotImplementedError()

    def flatten(self):
        self._geom.FlattenTo2D()

    def geoms(self) -> Iterator[ogr.Geometry]:
        # Returns all *geometries* of the geometry
        count = self._geom.GetGeometryCount()

        if count == 0:
            yield self._geom
            return

        for i in range(count):
            geom = self._geom.GetGeometryRef(i)

            if geom.GetGeometryType() == ogr.wkbLinearRing:
                yield geom.GetCurveGeometry()
            else:
                yield geom

    def points(self) -> Iterator[ogr.Geometry]:
        # Returns all *vertices* of the geometry (as ogr.wkbPoint25D)
        geoms = [n for n in self.geoms()]
        geoms.reverse()

        while len(geoms) > 0:
            g = geoms.pop()

            if g.GetGeometryCount() > 0:
                geoms.append(g)
                continue

            for i in range(g.GetPointCount()):
                yield self._ogr_point(g.GetPoint(i))

    def equals(self, other: BaseGeometry) -> bool:
        if self is other:
            return True

        if not isinstance(other, self.__class__):
            return False

        sg, og = self._geom, other._geom

        if sg.GetGeometryCount() != og.GetGeometryCount():
            return False

        if sg.GetGeometryCount() == 0 and sg.GetPointCount() != og.GetPointCount():
            return False

        return sg.Within(og) and og.Within(sg)

    def update_attributes(self, data_or_geom: dict | BaseGeometry):
        if isinstance(data_or_geom, BaseGeometry):
            self._attrs.update(data_or_geom._attrs)
        else:
            self._attrs.update(data_or_geom)

    def set_layer(self, layer: str):
        self._layer = layer

    def set_crs(self, crs):
        if crs is None:
            return

        crs = get_global_crs(crs)

        if self._crs is crs:
            return

        if self._crs is None:
            self._geom.AssignSpatialReference(crs)
        else:
            if self._crs.EPSGTreatsAsNorthingEasting():
                self._geom.SwapXY()

            self._geom.Transform(get_transformation(self._crs, crs))

            if crs.EPSGTreatsAsNorthingEasting() or crs.EPSGTreatsAsLatLong():
                self._geom.SwapXY()

        self._crs = crs
