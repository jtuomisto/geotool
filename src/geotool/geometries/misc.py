from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geopoint import GeoPoint
from .geoline import GeoLine
from .geopolygon import GeoPolygon
from .geomultipoint import GeoMultiPoint
from .geomultiline import GeoMultiLine
from .geomultipolygon import GeoMultiPolygon
from .geocollection import GeoCollection
from .geocircularstring import GeoCircularString
from .geocompoundcurve import GeoCompoundCurve
from .geocurvepolygon import GeoCurvePolygon


OGR_TO_GEO = {
    ogr.wkbPoint: GeoPoint,
    ogr.wkbLineString: GeoLine,
    ogr.wkbPolygon: GeoPolygon,
    ogr.wkbMultiPoint: GeoMultiPoint,
    ogr.wkbMultiLineString: GeoMultiLine,
    ogr.wkbMultiPolygon: GeoMultiPolygon,
    ogr.wkbGeometryCollection: GeoCollection,
    ogr.wkbCircularString: GeoCircularString,
    ogr.wkbCompoundCurve: GeoCompoundCurve,
    ogr.wkbCurvePolygon: GeoCurvePolygon
}

COERCE_LINEAR = {
    GeoCurvePolygon: GeoPolygon,
    GeoCompoundCurve: GeoLine,
    GeoCircularString: GeoLine
}


def get_geo_from_ogr(geom: ogr.Geometry | int) -> BaseGeometry | type:
    is_geometry = isinstance(geom, ogr.Geometry)

    if is_geometry:
        geom_type = geom.GetGeometryType()
    else:
        geom_type = geom

    geom_type = ogr.GT_Flatten(geom_type)

    if (geo_cls := OGR_TO_GEO.get(geom_type)) is None:
        raise GeometryError('Unknown geometry type', geom_type)

    if is_geometry:
        return geo_cls(geometry=geom)
    else:
        return geo_cls


def coerce_geometry(g: BaseGeometry, geo_cls: type) -> BaseGeometry:
    orig_cls = g.__class__

    if orig_cls is geo_cls or geo_cls is None:
        return g

    if COERCE_LINEAR.get(orig_cls) is geo_cls:
        return g.to_linear()

    geom = ogr.ForceTo(g._geom, geo_cls._geometry_type)

    if not ogr.GT_IsSubClassOf(geom.GetGeometryType(), geo_cls._geometry_type):
        raise GeometryError('Cannot coerce geometries', orig_cls.__name__, geo_cls.__name__)

    new_geo = get_geo_from_ogr(geom)
    new_geo.set_crs(g.crs)
    new_geo.set_layer(g.layer)
    new_geo.update_attributes(g)

    return new_geo


def geom_is_2D(geom: ogr.Geometry | int) -> bool:
    geom_type = geom

    if isinstance(geom, ogr.Geometry):
        geom_type = geom.GetGeometryType()

    return not ogr.GT_HasZ(geom_type)
