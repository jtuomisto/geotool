from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geopoint import GeoPoint


class GeoLine(BaseGeometry):
    _geometry_type = ogr.wkbLineString

    def __len__(self) -> int:
        return self._geom.GetPointCount()

    @property
    def as_ring(self) -> ogr.Geometry:
        if not self._geom.IsRing():
            raise GeometryError('Line does not form a closed ring')

        # ogr can't create linear rings from wkb or wkt
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AssignSpatialReference(self._crs)

        for points in self._geom.GetPoints():
            ring.AddPoint(*points)

        return ring

    def append(self, point_or_north: GeoPoint | float, east: float=None, elev: float=None):
        if isinstance(point_or_north, GeoPoint):
            point_or_north.set_crs(self._crs)
            self._geom.AddPoint(*point_or_north.as_tuple)
        else:
            self._geom.AddPoint(float(east), float(point_or_north), 0.0 if elev is None else float(elev))
