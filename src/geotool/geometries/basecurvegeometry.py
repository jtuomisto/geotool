from abc import ABC, abstractmethod
from osgeo import ogr
from .basegeometry import BaseGeometry


MAX_ANGLE_STEP = 2.5
LINEAR_OPTS = ['ADD_INTERMEDIATE_POINT=NO']


class BaseCurveGeometry(ABC):
    def _linearize(self, geom: ogr.Geometry):
        return geom.GetLinearGeometry(MAX_ANGLE_STEP, options=LINEAR_OPTS)

    @abstractmethod
    def to_linear(self) -> BaseGeometry:
        raise NotImplementedError()
