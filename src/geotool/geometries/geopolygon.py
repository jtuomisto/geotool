from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geoline import GeoLine


class GeoPolygon(BaseGeometry):
    _geometry_type = ogr.wkbPolygon

    def append(self, geom: GeoLine):
        if not isinstance(geom, GeoLine):
            raise TypeError('Polygon accepts only lines')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom.as_ring)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append ring to polygon', success)
