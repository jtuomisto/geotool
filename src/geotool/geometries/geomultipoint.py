from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geopoint import GeoPoint


class GeoMultiPoint(BaseGeometry):
    _geometry_type = ogr.wkbMultiPoint

    def append(self, geom: GeoPoint):
        if not isinstance(geom, GeoPoint):
            raise TypeError('Multi-point accepts only points')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append point to multi-point', success)
