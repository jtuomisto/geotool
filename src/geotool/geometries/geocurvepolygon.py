from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .basecurvegeometry import BaseCurveGeometry
from .geoline import GeoLine
from .geopolygon import GeoPolygon
from .geocircularstring import GeoCircularString
from .geocompoundcurve import GeoCompoundCurve


class GeoCurvePolygon(BaseGeometry, BaseCurveGeometry):
    _geometry_type = ogr.wkbCurvePolygon

    def append(self, geom: GeoLine | GeoCircularString | GeoCompoundCurve):
        if not isinstance(geom, GeoLine | GeoCircularString | GeoCompoundCurve):
            raise TypeError('Curve polygon accepts only lines, circular strings and compound curves')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append ring to curve polygon', success)

    def to_linear(self) -> GeoPolygon:
        linear = self._linearize(self._geom)
        g = GeoPolygon(geometry=linear)
        g.set_crs(self._crs)
        g.set_layer(self._layer)
        g.update_attributes(self)

        return g
