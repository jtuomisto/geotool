from osgeo import ogr
from geotool.errors import GeometryError
from .basegeometry import BaseGeometry
from .geoline import GeoLine


class GeoMultiLine(BaseGeometry):
    _geometry_type = ogr.wkbMultiLineString

    def append(self, geom: GeoLine):
        if not isinstance(geom, GeoLine):
            raise TypeError('Multi-line accepts only lines')

        geom.set_crs(self._crs)

        if (success := self._geom.AddGeometryDirectly(geom._geom)) != ogr.OGRERR_NONE:
            raise GeometryError('Failed to append line to multi-line', success)
