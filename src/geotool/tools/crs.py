from osgeo import osr


# These methods exist for performance reasons, comparing two coordinate reference systems
# for equality seems to be somewhat expensive.
# Comparing their identity instead is an order of magnitude faster.


CRS_MAP = {}
CRS_TRANS_MAP = {}


def get_global_crs(crs: int | str | osr.SpatialReference) -> osr.SpatialReference:
    if crs is None:
        return None

    is_srs = isinstance(crs, osr.SpatialReference)

    if is_srs:
        crs_name = crs.GetName().upper()
    elif isinstance(crs, str):
        crs_name = crs.upper()
    elif isinstance(crs, int):
        crs_name = f'EPSG:{crs}'
    else:
        return None

    if crs_name in CRS_MAP:
        return CRS_MAP[crs_name]

    try:
        if is_srs:
            c = crs
        else:
            c = osr.SpatialReference()
            c.SetFromUserInput(crs_name)

        c.AutoIdentifyEPSG()
    except RuntimeError as e:
        raise ValueError(f'Unsupported CRS: {crs_name}') from e

    auth, code = c.GetAttrValue('AUTHORITY'), c.GetAuthorityCode(None)
    crs_auth_code = f'{auth}:{code}'.upper()

    if crs_auth_code in CRS_MAP:
        CRS_MAP[crs_name] = CRS_MAP[crs_auth_code]
    else:
        CRS_MAP[crs_name] = c
        CRS_MAP[crs_auth_code] = c

    return CRS_MAP[crs_name]


def get_transformation(from_crs: osr.SpatialReference, to_crs: osr.SpatialReference):
    key = (from_crs, to_crs)

    if (transformation := CRS_TRANS_MAP.get(key)) is not None:
        return transformation

    return CRS_TRANS_MAP.setdefault(key, osr.CoordinateTransformation(from_crs, to_crs))


def urn_string_to_crs(urn: str) -> osr.SpatialReference:
    parts = urn.split(':')

    if len(parts) != 7:
        raise ValueError(f'Invalid URN: {urn}')

    _, _, _, obj, auth, _, code = parts

    if obj.lower() != 'crs':
        raise ValueError(f'Invalid URN type: {obj}')

    return get_global_crs(f'{auth}:{code}')


def crs_to_urn_string(crs: osr.SpatialReference) -> str:
    auth, code = crs.GetAttrValue('AUTHORITY'), crs.GetAuthorityCode(None)
    return f'urn:ogc:def:crs:{auth}::{code}'
