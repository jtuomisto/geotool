import unicodedata


def string_to_ascii(string: str, repl: str='_') -> str:
    asc = []

    for letter in string:
        if letter == repl:
            asc.append(repl)
            continue

        code_point = ord(letter)

        # Regular ASCII printable characters
        if 32 <= code_point <= 126:
            asc.append(letter)
            continue

        decomp = unicodedata.decomposition(letter)

        if len(decomp) == 0:
            asc.append(repl)
            continue

        decomp_code_point, _, _ = decomp.partition(' ')

        try:
            dcp = int(decomp_code_point, 16)
        except ValueError:
            asc.append(repl)
            continue

        if 32 <= dcp <= 126:
            asc.append(chr(dcp))
        else:
            asc.append(repl)

    return ''.join(asc)
