class Counter:
    __slots__ = ['_counter']

    def __init__(self, start: int=1):
        self._counter = start

    def __call__(self) -> int:
        c, self._counter = self._counter, self._counter + 1
        return c

    def reset(self, start: int=1) -> int:
        self._counter = start
        return start

    def get(self) -> int:
        return self._counter
