geotool
=======

Tool to read/write GIS-data in GT, CSV, GeoJSON, GeoPackage and other formats.

Also handles some Finnish governmental data sources:

- XML KTJ-data from [Maanmittauslaitos][MMLktj].
- CSV real estate tax files from [Verohallinto][vero].

Dependencies
------------

Requires Python 3.11 or later.

Some dependencies might not have pre-built wheels in pypi, but Windows builds are available here:

- [lxml](https://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml)
- [GDAL](https://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal)

[MMLktj]: https://www.maanmittauslaitos.fi/huoneistot-ja-kiinteistot/asiantuntevalle-kayttajalle/kiinteistotiedot-ja-niiden-hankinta
[vero]: https://www.vero.fi/
